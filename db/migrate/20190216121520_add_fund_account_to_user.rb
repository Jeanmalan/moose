class AddFundAccountToUser < ActiveRecord::Migration[5.2]
  def change
    add_column return_table_name, :fund_account, :integer
  end

  def return_table_name
    "peregrine.agc_definition_table"
  end
  
end

