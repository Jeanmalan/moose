# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)


# <template>
#   <div>
#     <chart :chart-data="datacollection"></chart>
#     <div class="table-small">
#       <table class="table" style="background-color:rgba(0,0,0,0);border-radius: 10px;overflow:scroll;height:700px !important;display: block;">
#         <thead>
#           <tr>
#           <th style="font-weight:bold">Date</th>
#           <th style="font-weight:bold">Fund account</th>
#           <th style="font-weight:bold">Fund Name</th>
#           <th style="font-weight:bold">Quantity</th>
#           <th style="font-weight:bold">Price</th>
#           <th style="font-weight:bold">Wac Native</th>
#           <th style="font-weight:bold">Instrument Code Name</th>
#           </tr>
#         </thead>

#         <tbody>
#           <tr v-for="pv in portfolio_valuations"  :key="pv.id">
#             <td>{{pv.valuation_date.substring(0,10)}}</td>
#             <td>{{ pv.fund_account }}</td>
#             <td>{{ pv.fund_name }}</td>
#             <td>{{ pv.quantity }}</td>
#             <td>{{ pv.price }}</td>
#             <td :class="[ pv.wac_native>= 0 ? 'font-green' : 'font-red' ]">{{ pv.wac_native }}</td>
#             <td>{{ pv.instrument_code_name }}</td>
#           </tr>
#         </tbody>
#       </table>
#     </div>
#   </div>
# </template>

# <script>
# import Chart from "chart.js";
# export default {
#   props: ["portfolio_valuations"],
#   components: { Chart },
#   data() {
#     return {
#       datacollection: null
#     };
#   },
#   mounted() {
#     this.fillData();
#   },
#   methods: {
#     fillData() {
#       this.datacollection = {
#         labels: [
#           "January",
#           "February",
#           "March",
#           "April",
#           "May",
#           "June",
#           "July",
#           "August",
#           "September",
#           "October",
#           "November",
#           "December"
#         ],
#         datasets: [
#           {
#             label: "GitHub Commits",
#             backgroundColor: "#f87979",
#             data: [70, 20, 12, 39, 100, 40, 95, 80, 80, 20, 12, 101]
#           },
#           {
#             label: "Monthly incomes",
#             backgroundColor: "#A5CC82",
#             data: [205, 408, 188, 190, 58, 200, 190, 400, 164, 254, 290, 201]
#           }
#         ]
#       };
#     }
#   }
# };
# </script>

# <style scoped>
# </style>