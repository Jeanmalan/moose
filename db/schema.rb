# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_18_014012) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "financial_transactions", force: :cascade do |t|
    t.integer "TransactionId"
    t.integer "FundAccount"
    t.string "FundName"
    t.datetime "TransactionDateTime"
    t.integer "TransactionCode"
    t.string "CurrencyCode"
    t.integer "CashAccount"
    t.datetime "ValueDate"
    t.datetime "PayDate"
    t.float "Amount"
    t.string "TransactionDescription"
    t.string "Reversal"
    t.integer "ReversedTransactionId"
    t.string "BDABranchCode"
    t.string "BDAPartnerCode"
    t.string "InstrumentCode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "financial_transactionts", force: :cascade do |t|
    t.integer "transaction_id"
    t.integer "fund_account"
    t.string "fund_name"
    t.datetime "transaction_date_time"
    t.integer "transaction_code"
    t.string "currency_code"
    t.integer "cash_account"
    t.datetime "value_date"
    t.datetime "pay_date"
    t.float "amount"
    t.string "transaction_description"
    t.string "reversal"
    t.integer "reversed_transaction_id"
    t.string "bdabranch_code"
    t.string "bdapartner_code"
    t.string "instrument_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profit_and_losses", force: :cascade do |t|
    t.date "date"
    t.integer "fund_account"
    t.float "inflow"
    t.float "outflow"
    t.float "opening_balance"
    t.float "closing_balance"
    t.float "daily_pnl"
    t.float "cum_pnl"
    t.float "daily_percent"
    t.float "cum_percent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "fund_account"
    t.string "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
