require "application_system_test_case"

class AgcDefinitionTablesTest < ApplicationSystemTestCase
  setup do
    @agc_definition_table = agc_definition_tables(:one)
  end

  test "visiting the index" do
    visit agc_definition_tables_url
    assert_selector "h1", text: "Agc Definition Tables"
  end

  test "creating a Agc definition table" do
    visit agc_definition_tables_url
    click_on "New Agc Definition Table"

    fill_in "Client name", with: @agc_definition_table.client_name
    fill_in "Created at", with: @agc_definition_table.created_at
    fill_in "Deleted at", with: @agc_definition_table.deleted_at
    fill_in "Fund account", with: @agc_definition_table.fund_account
    fill_in "Fund name", with: @agc_definition_table.fund_name
    fill_in "Grouping1", with: @agc_definition_table.grouping1
    fill_in "Grouping2", with: @agc_definition_table.grouping2
    fill_in "Grouping3", with: @agc_definition_table.grouping3
    fill_in "Grouping4", with: @agc_definition_table.grouping4
    fill_in "Grouping5", with: @agc_definition_table.grouping5
    fill_in "Id", with: @agc_definition_table.id
    fill_in "Iress nr", with: @agc_definition_table.iress_nr
    fill_in "Strategy", with: @agc_definition_table.strategy
    fill_in "Trader", with: @agc_definition_table.trader
    fill_in "Updated at", with: @agc_definition_table.updated_at
    click_on "Create Agc definition table"

    assert_text "Agc definition table was successfully created"
    click_on "Back"
  end

  test "updating a Agc definition table" do
    visit agc_definition_tables_url
    click_on "Edit", match: :first

    fill_in "Client name", with: @agc_definition_table.client_name
    fill_in "Created at", with: @agc_definition_table.created_at
    fill_in "Deleted at", with: @agc_definition_table.deleted_at
    fill_in "Fund account", with: @agc_definition_table.fund_account
    fill_in "Fund name", with: @agc_definition_table.fund_name
    fill_in "Grouping1", with: @agc_definition_table.grouping1
    fill_in "Grouping2", with: @agc_definition_table.grouping2
    fill_in "Grouping3", with: @agc_definition_table.grouping3
    fill_in "Grouping4", with: @agc_definition_table.grouping4
    fill_in "Grouping5", with: @agc_definition_table.grouping5
    fill_in "Id", with: @agc_definition_table.id
    fill_in "Iress nr", with: @agc_definition_table.iress_nr
    fill_in "Strategy", with: @agc_definition_table.strategy
    fill_in "Trader", with: @agc_definition_table.trader
    fill_in "Updated at", with: @agc_definition_table.updated_at
    click_on "Update Agc definition table"

    assert_text "Agc definition table was successfully updated"
    click_on "Back"
  end

  test "destroying a Agc definition table" do
    visit agc_definition_tables_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Agc definition table was successfully destroyed"
  end
end
