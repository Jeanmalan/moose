require "application_system_test_case"

class PortfolioValuationsTest < ApplicationSystemTestCase
  setup do
    @portfolio_valuation = portfolio_valuations(:one)
  end

  test "visiting the index" do
    visit portfolio_valuations_url
    assert_selector "h1", text: "Portfolio Valuations"
  end

  test "creating a Portfolio valuation" do
    visit portfolio_valuations_url
    click_on "New Portfolio Valuation"

    fill_in "Bda branch code", with: @portfolio_valuation.bda_branch_code
    fill_in "Bda partner code", with: @portfolio_valuation.bda_partner_code
    fill_in "Currency code", with: @portfolio_valuation.currency_code
    fill_in "Fund account", with: @portfolio_valuation.fund_account
    fill_in "Fund name", with: @portfolio_valuation.fund_name
    fill_in "Industry", with: @portfolio_valuation.industry
    fill_in "Instrument code", with: @portfolio_valuation.instrument_code
    fill_in "Instrument code name", with: @portfolio_valuation.instrument_code_name
    fill_in "Instrument type", with: @portfolio_valuation.instrument_type
    fill_in "Instrument version", with: @portfolio_valuation.instrument_version
    fill_in "Isin", with: @portfolio_valuation.isin
    fill_in "Price", with: @portfolio_valuation.price
    fill_in "Quantity", with: @portfolio_valuation.quantity
    fill_in "Sector", with: @portfolio_valuation.sector
    fill_in "Valuation date", with: @portfolio_valuation.valuation_date
    fill_in "Wac native", with: @portfolio_valuation.wac_native
    click_on "Create Portfolio valuation"

    assert_text "Portfolio valuation was successfully created"
    click_on "Back"
  end

  test "updating a Portfolio valuation" do
    visit portfolio_valuations_url
    click_on "Edit", match: :first

    fill_in "Bda branch code", with: @portfolio_valuation.bda_branch_code
    fill_in "Bda partner code", with: @portfolio_valuation.bda_partner_code
    fill_in "Currency code", with: @portfolio_valuation.currency_code
    fill_in "Fund account", with: @portfolio_valuation.fund_account
    fill_in "Fund name", with: @portfolio_valuation.fund_name
    fill_in "Industry", with: @portfolio_valuation.industry
    fill_in "Instrument code", with: @portfolio_valuation.instrument_code
    fill_in "Instrument code name", with: @portfolio_valuation.instrument_code_name
    fill_in "Instrument type", with: @portfolio_valuation.instrument_type
    fill_in "Instrument version", with: @portfolio_valuation.instrument_version
    fill_in "Isin", with: @portfolio_valuation.isin
    fill_in "Price", with: @portfolio_valuation.price
    fill_in "Quantity", with: @portfolio_valuation.quantity
    fill_in "Sector", with: @portfolio_valuation.sector
    fill_in "Valuation date", with: @portfolio_valuation.valuation_date
    fill_in "Wac native", with: @portfolio_valuation.wac_native
    click_on "Update Portfolio valuation"

    assert_text "Portfolio valuation was successfully updated"
    click_on "Back"
  end

  test "destroying a Portfolio valuation" do
    visit portfolio_valuations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Portfolio valuation was successfully destroyed"
  end
end
