require "application_system_test_case"

class ClientCategoriesTest < ApplicationSystemTestCase
  setup do
    @client_category = client_categories(:one)
  end

  test "visiting the index" do
    visit client_categories_url
    assert_selector "h1", text: "Client Categories"
  end

  test "creating a Client category" do
    visit client_categories_url
    click_on "New Client Category"

    fill_in "Client category", with: @client_category.client_category
    fill_in "Fund account", with: @client_category.fund_account
    click_on "Create Client category"

    assert_text "Client category was successfully created"
    click_on "Back"
  end

  test "updating a Client category" do
    visit client_categories_url
    click_on "Edit", match: :first

    fill_in "Client category", with: @client_category.client_category
    fill_in "Fund account", with: @client_category.fund_account
    click_on "Update Client category"

    assert_text "Client category was successfully updated"
    click_on "Back"
  end

  test "destroying a Client category" do
    visit client_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Client category was successfully destroyed"
  end
end
