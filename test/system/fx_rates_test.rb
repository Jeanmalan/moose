require "application_system_test_case"

class FxRatesTest < ApplicationSystemTestCase
  setup do
    @fx_rate = fx_rates(:one)
  end

  test "visiting the index" do
    visit fx_rates_url
    assert_selector "h1", text: "Fx Rates"
  end

  test "creating a Fx rate" do
    visit fx_rates_url
    click_on "New Fx Rate"

    fill_in "Exchange rate", with: @fx_rate.exchange_rate
    fill_in "Source currency", with: @fx_rate.source_currency
    fill_in "Target currency", with: @fx_rate.target_currency
    fill_in "Valuation date", with: @fx_rate.valuation_date
    click_on "Create Fx rate"

    assert_text "Fx rate was successfully created"
    click_on "Back"
  end

  test "updating a Fx rate" do
    visit fx_rates_url
    click_on "Edit", match: :first

    fill_in "Exchange rate", with: @fx_rate.exchange_rate
    fill_in "Source currency", with: @fx_rate.source_currency
    fill_in "Target currency", with: @fx_rate.target_currency
    fill_in "Valuation date", with: @fx_rate.valuation_date
    click_on "Update Fx rate"

    assert_text "Fx rate was successfully updated"
    click_on "Back"
  end

  test "destroying a Fx rate" do
    visit fx_rates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Fx rate was successfully destroyed"
  end
end
