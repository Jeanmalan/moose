require "application_system_test_case"

class YearToDatesTest < ApplicationSystemTestCase
  setup do
    @year_to_date = year_to_dates(:one)
  end

  test "visiting the index" do
    visit year_to_dates_url
    assert_selector "h1", text: "Year To Dates"
  end

  test "creating a Year to date" do
    visit year_to_dates_url
    click_on "New Year To Date"

    fill_in "Date", with: @year_to_date.date
    fill_in "Fund account", with: @year_to_date.fund_account
    fill_in "Value", with: @year_to_date.value
    click_on "Create Year to date"

    assert_text "Year to date was successfully created"
    click_on "Back"
  end

  test "updating a Year to date" do
    visit year_to_dates_url
    click_on "Edit", match: :first

    fill_in "Date", with: @year_to_date.date
    fill_in "Fund account", with: @year_to_date.fund_account
    fill_in "Value", with: @year_to_date.value
    click_on "Update Year to date"

    assert_text "Year to date was successfully updated"
    click_on "Back"
  end

  test "destroying a Year to date" do
    visit year_to_dates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Year to date was successfully destroyed"
  end
end
