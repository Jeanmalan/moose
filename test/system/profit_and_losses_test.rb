require "application_system_test_case"

class ProfitAndLossesTest < ApplicationSystemTestCase
  setup do
    @profit_and_loss = profit_and_losses(:one)
  end

  test "visiting the index" do
    visit profit_and_losses_url
    assert_selector "h1", text: "Profit And Losses"
  end

  test "creating a Profit and loss" do
    visit profit_and_losses_url
    click_on "New Profit And Loss"

    fill_in "Closing balance", with: @profit_and_loss.closing_balance
    fill_in "Cum percent", with: @profit_and_loss.cum_percent
    fill_in "Cum pnl", with: @profit_and_loss.cum_pnl
    fill_in "Daily percent", with: @profit_and_loss.daily_percent
    fill_in "Daily pnl", with: @profit_and_loss.daily_pnl
    fill_in "Date datetime", with: @profit_and_loss.date_datetime
    fill_in "Fund account", with: @profit_and_loss.fund_account
    fill_in "Inflow", with: @profit_and_loss.inflow
    fill_in "Nav", with: @profit_and_loss.nav
    fill_in "Opening balance", with: @profit_and_loss.opening_balance
    fill_in "Outflow", with: @profit_and_loss.outflow
    click_on "Create Profit and loss"

    assert_text "Profit and loss was successfully created"
    click_on "Back"
  end

  test "updating a Profit and loss" do
    visit profit_and_losses_url
    click_on "Edit", match: :first

    fill_in "Closing balance", with: @profit_and_loss.closing_balance
    fill_in "Cum percent", with: @profit_and_loss.cum_percent
    fill_in "Cum pnl", with: @profit_and_loss.cum_pnl
    fill_in "Daily percent", with: @profit_and_loss.daily_percent
    fill_in "Daily pnl", with: @profit_and_loss.daily_pnl
    fill_in "Date datetime", with: @profit_and_loss.date_datetime
    fill_in "Fund account", with: @profit_and_loss.fund_account
    fill_in "Inflow", with: @profit_and_loss.inflow
    fill_in "Nav", with: @profit_and_loss.nav
    fill_in "Opening balance", with: @profit_and_loss.opening_balance
    fill_in "Outflow", with: @profit_and_loss.outflow
    click_on "Update Profit and loss"

    assert_text "Profit and loss was successfully updated"
    click_on "Back"
  end

  test "destroying a Profit and loss" do
    visit profit_and_losses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Profit and loss was successfully destroyed"
  end
end
