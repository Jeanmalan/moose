require "application_system_test_case"

class TurnoversTest < ApplicationSystemTestCase
  setup do
    @turnover = turnovers(:one)
  end

  test "visiting the index" do
    visit turnovers_url
    assert_selector "h1", text: "Turnovers"
  end

  test "creating a Turnover" do
    visit turnovers_url
    click_on "New Turnover"

    fill_in "Amount", with: @turnover.amount
    fill_in "Month", with: @turnover.month
    fill_in "Year", with: @turnover.year
    click_on "Create Turnover"

    assert_text "Turnover was successfully created"
    click_on "Back"
  end

  test "updating a Turnover" do
    visit turnovers_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @turnover.amount
    fill_in "Month", with: @turnover.month
    fill_in "Year", with: @turnover.year
    click_on "Update Turnover"

    assert_text "Turnover was successfully updated"
    click_on "Back"
  end

  test "destroying a Turnover" do
    visit turnovers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Turnover was successfully destroyed"
  end
end
