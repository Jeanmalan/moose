require "application_system_test_case"

class InstrumentsTest < ApplicationSystemTestCase
  setup do
    @instrument = instruments(:one)
  end

  test "visiting the index" do
    visit instruments_url
    assert_selector "h1", text: "Instruments"
  end

  test "creating a Instrument" do
    visit instruments_url
    click_on "New Instrument"

    fill_in "Currency code", with: @instrument.currency_code
    fill_in "Exchange", with: @instrument.exchange
    fill_in "Industry", with: @instrument.industry
    fill_in "Instrument code", with: @instrument.instrument_code
    fill_in "Instrument name", with: @instrument.instrument_name
    fill_in "Isin", with: @instrument.isin
    fill_in "Sector", with: @instrument.sector
    fill_in "Sedol", with: @instrument.sedol
    fill_in "Valuation date", with: @instrument.valuation_date
    click_on "Create Instrument"

    assert_text "Instrument was successfully created"
    click_on "Back"
  end

  test "updating a Instrument" do
    visit instruments_url
    click_on "Edit", match: :first

    fill_in "Currency code", with: @instrument.currency_code
    fill_in "Exchange", with: @instrument.exchange
    fill_in "Industry", with: @instrument.industry
    fill_in "Instrument code", with: @instrument.instrument_code
    fill_in "Instrument name", with: @instrument.instrument_name
    fill_in "Isin", with: @instrument.isin
    fill_in "Sector", with: @instrument.sector
    fill_in "Sedol", with: @instrument.sedol
    fill_in "Valuation date", with: @instrument.valuation_date
    click_on "Update Instrument"

    assert_text "Instrument was successfully updated"
    click_on "Back"
  end

  test "destroying a Instrument" do
    visit instruments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Instrument was successfully destroyed"
  end
end
