require "application_system_test_case"

class MonthToDatesTest < ApplicationSystemTestCase
  setup do
    @month_to_date = month_to_dates(:one)
  end

  test "visiting the index" do
    visit month_to_dates_url
    assert_selector "h1", text: "Month To Dates"
  end

  test "creating a Month to date" do
    visit month_to_dates_url
    click_on "New Month To Date"

    fill_in "Date", with: @month_to_date.date
    fill_in "Fund account", with: @month_to_date.fund_account
    fill_in "Value", with: @month_to_date.value
    click_on "Create Month to date"

    assert_text "Month to date was successfully created"
    click_on "Back"
  end

  test "updating a Month to date" do
    visit month_to_dates_url
    click_on "Edit", match: :first

    fill_in "Date", with: @month_to_date.date
    fill_in "Fund account", with: @month_to_date.fund_account
    fill_in "Value", with: @month_to_date.value
    click_on "Update Month to date"

    assert_text "Month to date was successfully updated"
    click_on "Back"
  end

  test "destroying a Month to date" do
    visit month_to_dates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Month to date was successfully destroyed"
  end
end
