require "application_system_test_case"

class InstrumentMarginLevelsTest < ApplicationSystemTestCase
  setup do
    @instrument_margin_level = instrument_margin_levels(:one)
  end

  test "visiting the index" do
    visit instrument_margin_levels_url
    assert_selector "h1", text: "Instrument Margin Levels"
  end

  test "creating a Instrument margin level" do
    visit instrument_margin_levels_url
    click_on "New Instrument Margin Level"

    fill_in "Exchange", with: @instrument_margin_level.exchange
    fill_in "Instrument code", with: @instrument_margin_level.instrument_code
    fill_in "Instrument level", with: @instrument_margin_level.instrument_level
    click_on "Create Instrument margin level"

    assert_text "Instrument margin level was successfully created"
    click_on "Back"
  end

  test "updating a Instrument margin level" do
    visit instrument_margin_levels_url
    click_on "Edit", match: :first

    fill_in "Exchange", with: @instrument_margin_level.exchange
    fill_in "Instrument code", with: @instrument_margin_level.instrument_code
    fill_in "Instrument level", with: @instrument_margin_level.instrument_level
    click_on "Update Instrument margin level"

    assert_text "Instrument margin level was successfully updated"
    click_on "Back"
  end

  test "destroying a Instrument margin level" do
    visit instrument_margin_levels_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Instrument margin level was successfully destroyed"
  end
end
