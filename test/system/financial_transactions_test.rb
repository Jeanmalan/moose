require "application_system_test_case"

class FinancialTransactionsTest < ApplicationSystemTestCase
  setup do
    @financial_transaction = financial_transactions(:one)
  end

  test "visiting the index" do
    visit financial_transactions_url
    assert_selector "h1", text: "Financial Transactions"
  end

  test "creating a Financial transaction" do
    visit financial_transactions_url
    click_on "New Financial Transaction"

    fill_in "Amount", with: @financial_transaction.amount
    fill_in "Bdabranch code", with: @financial_transaction.bdabranch_code
    fill_in "Bdapartner code", with: @financial_transaction.bdapartner_code
    fill_in "Cash account", with: @financial_transaction.cash_account
    fill_in "Currency code", with: @financial_transaction.currency_code
    fill_in "Fund account", with: @financial_transaction.fund_account
    fill_in "Fund name", with: @financial_transaction.fund_name
    fill_in "Instrument code", with: @financial_transaction.instrument_code
    fill_in "Pay date", with: @financial_transaction.pay_date
    fill_in "Reversal", with: @financial_transaction.reversal
    fill_in "Reversed transaction", with: @financial_transaction.reversed_transaction_id
    fill_in "Transaction code", with: @financial_transaction.transaction_code
    fill_in "Transaction date time", with: @financial_transaction.transaction_date_time
    fill_in "Transaction description", with: @financial_transaction.transaction_description
    fill_in "Transaction", with: @financial_transaction.transaction_id
    fill_in "Value date", with: @financial_transaction.value_date
    click_on "Create Financial transaction"

    assert_text "Financial transaction was successfully created"
    click_on "Back"
  end

  test "updating a Financial transaction" do
    visit financial_transactions_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @financial_transaction.amount
    fill_in "Bdabranch code", with: @financial_transaction.bdabranch_code
    fill_in "Bdapartner code", with: @financial_transaction.bdapartner_code
    fill_in "Cash account", with: @financial_transaction.cash_account
    fill_in "Currency code", with: @financial_transaction.currency_code
    fill_in "Fund account", with: @financial_transaction.fund_account
    fill_in "Fund name", with: @financial_transaction.fund_name
    fill_in "Instrument code", with: @financial_transaction.instrument_code
    fill_in "Pay date", with: @financial_transaction.pay_date
    fill_in "Reversal", with: @financial_transaction.reversal
    fill_in "Reversed transaction", with: @financial_transaction.reversed_transaction_id
    fill_in "Transaction code", with: @financial_transaction.transaction_code
    fill_in "Transaction date time", with: @financial_transaction.transaction_date_time
    fill_in "Transaction description", with: @financial_transaction.transaction_description
    fill_in "Transaction", with: @financial_transaction.transaction_id
    fill_in "Value date", with: @financial_transaction.value_date
    click_on "Update Financial transaction"

    assert_text "Financial transaction was successfully updated"
    click_on "Back"
  end

  test "destroying a Financial transaction" do
    visit financial_transactions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Financial transaction was successfully destroyed"
  end
end
