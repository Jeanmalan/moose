require "application_system_test_case"

class ClientInformationsTest < ApplicationSystemTestCase
  setup do
    @client_information = client_informations(:one)
  end

  test "visiting the index" do
    visit client_informations_url
    assert_selector "h1", text: "Client Informations"
  end

  test "creating a Client information" do
    visit client_informations_url
    click_on "New Client Information"

    fill_in "Address line1", with: @client_information.address_line1
    fill_in "Address line2", with: @client_information.address_line2
    fill_in "Address line3", with: @client_information.address_line3
    fill_in "Address line4", with: @client_information.address_line4
    fill_in "Bda branch code", with: @client_information.bda_branch_code
    fill_in "Cgt individual", with: @client_information.cgt_individual
    fill_in "Client initials", with: @client_information.client_initials
    fill_in "Client surname", with: @client_information.client_surname
    fill_in "Client title", with: @client_information.client_title
    fill_in "Email address", with: @client_information.email_address
    fill_in "External trading account", with: @client_information.external_trading_account
    fill_in "First name", with: @client_information.first_name
    fill_in "Fund account", with: @client_information.fund_account
    fill_in "Fund name", with: @client_information.fund_name
    fill_in "Idno", with: @client_information.idno
    fill_in "Postal", with: @client_information.postal
    fill_in "Take on date", with: @client_information.take_on_date
    fill_in "Tax no", with: @client_information.tax_no
    fill_in "Tel no 1", with: @client_information.tel_no_1
    fill_in "Tel no 2", with: @client_information.tel_no_2
    fill_in "Vat no", with: @client_information.vat_no
    click_on "Create Client information"

    assert_text "Client information was successfully created"
    click_on "Back"
  end

  test "updating a Client information" do
    visit client_informations_url
    click_on "Edit", match: :first

    fill_in "Address line1", with: @client_information.address_line1
    fill_in "Address line2", with: @client_information.address_line2
    fill_in "Address line3", with: @client_information.address_line3
    fill_in "Address line4", with: @client_information.address_line4
    fill_in "Bda branch code", with: @client_information.bda_branch_code
    fill_in "Cgt individual", with: @client_information.cgt_individual
    fill_in "Client initials", with: @client_information.client_initials
    fill_in "Client surname", with: @client_information.client_surname
    fill_in "Client title", with: @client_information.client_title
    fill_in "Email address", with: @client_information.email_address
    fill_in "External trading account", with: @client_information.external_trading_account
    fill_in "First name", with: @client_information.first_name
    fill_in "Fund account", with: @client_information.fund_account
    fill_in "Fund name", with: @client_information.fund_name
    fill_in "Idno", with: @client_information.idno
    fill_in "Postal", with: @client_information.postal
    fill_in "Take on date", with: @client_information.take_on_date
    fill_in "Tax no", with: @client_information.tax_no
    fill_in "Tel no 1", with: @client_information.tel_no_1
    fill_in "Tel no 2", with: @client_information.tel_no_2
    fill_in "Vat no", with: @client_information.vat_no
    click_on "Update Client information"

    assert_text "Client information was successfully updated"
    click_on "Back"
  end

  test "destroying a Client information" do
    visit client_informations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Client information was successfully destroyed"
  end
end
