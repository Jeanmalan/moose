require "application_system_test_case"

class IressPortfolioCashDetailsTest < ApplicationSystemTestCase
  setup do
    @iress_portfolio_cash_detail = iress_portfolio_cash_details(:one)
  end

  test "visiting the index" do
    visit iress_portfolio_cash_details_url
    assert_selector "h1", text: "Iress Portfolio Cash Details"
  end

  test "creating a Iress portfolio cash detail" do
    visit iress_portfolio_cash_details_url
    click_on "New Iress Portfolio Cash Detail"

    fill_in "Cash balance", with: @iress_portfolio_cash_detail.cash_balance
    fill_in "Clearing house margin", with: @iress_portfolio_cash_detail.clearing_house_margin
    fill_in "Create date time", with: @iress_portfolio_cash_detail.create_date_time
    fill_in "Created at", with: @iress_portfolio_cash_detail.created_at
    fill_in "Currency code", with: @iress_portfolio_cash_detail.currency_code
    fill_in "Deleted at", with: @iress_portfolio_cash_detail.deleted_at
    fill_in "External value", with: @iress_portfolio_cash_detail.external_value
    fill_in "Free equity", with: @iress_portfolio_cash_detail.free_equity
    fill_in "Glv", with: @iress_portfolio_cash_detail.glv
    fill_in "In market buy value", with: @iress_portfolio_cash_detail.in_market_buy_value
    fill_in "In market sell value", with: @iress_portfolio_cash_detail.in_market_sell_value
    fill_in "Margin lender total financed value", with: @iress_portfolio_cash_detail.margin_lender_total_financed_value
    fill_in "Net cash", with: @iress_portfolio_cash_detail.net_cash
    fill_in "Net unsettled buy value today", with: @iress_portfolio_cash_detail.net_unsettled_buy_value_today
    fill_in "Net unsettled sell value today", with: @iress_portfolio_cash_detail.net_unsettled_sell_value_today
    fill_in "Net unsettled value today", with: @iress_portfolio_cash_detail.net_unsettled_value_today
    fill_in "Option unsettled buy charges", with: @iress_portfolio_cash_detail.option_unsettled_buy_charges
    fill_in "Option unsettled buy value", with: @iress_portfolio_cash_detail.option_unsettled_buy_value
    fill_in "Option unsettled sell charges", with: @iress_portfolio_cash_detail.option_unsettled_sell_charges
    fill_in "Option unsettled sell value", with: @iress_portfolio_cash_detail.option_unsettled_sell_value
    fill_in "Portfolio cash code", with: @iress_portfolio_cash_detail.portfolio_cash_code
    fill_in "Portfolio cash name", with: @iress_portfolio_cash_detail.portfolio_cash_name
    fill_in "Portfolio code", with: @iress_portfolio_cash_detail.portfolio_code
    fill_in "Realized loss start of day value", with: @iress_portfolio_cash_detail.realized_loss_start_of_day_value
    fill_in "Total cfd collateral value", with: @iress_portfolio_cash_detail.total_cfd_collateral_value
    fill_in "Total cfd realised profit", with: @iress_portfolio_cash_detail.total_cfd_realised_profit
    fill_in "Total cfd realized profit in settlement currency", with: @iress_portfolio_cash_detail.total_cfd_realized_profit_in_settlement_currency
    fill_in "Total cfd unrealised profit", with: @iress_portfolio_cash_detail.total_cfd_unrealised_profit
    fill_in "Total initial margin", with: @iress_portfolio_cash_detail.total_initial_margin
    fill_in "Total non cfd market value", with: @iress_portfolio_cash_detail.total_non_cfd_market_value
    fill_in "Trust balance", with: @iress_portfolio_cash_detail.trust_balance
    fill_in "Unsettled buy charges", with: @iress_portfolio_cash_detail.unsettled_buy_charges
    fill_in "Unsettled buy value", with: @iress_portfolio_cash_detail.unsettled_buy_value
    fill_in "Unsettled sell charges", with: @iress_portfolio_cash_detail.unsettled_sell_charges
    fill_in "Unsettled sell value", with: @iress_portfolio_cash_detail.unsettled_sell_value
    fill_in "Update date time", with: @iress_portfolio_cash_detail.update_date_time
    fill_in "Updated at", with: @iress_portfolio_cash_detail.updated_at
    fill_in "Upload source", with: @iress_portfolio_cash_detail.upload_source
    fill_in "Version stamp", with: @iress_portfolio_cash_detail.version_stamp
    fill_in "Yesterday equity sell charges", with: @iress_portfolio_cash_detail.yesterday_equity_sell_charges
    fill_in "Yesterday equity sell value", with: @iress_portfolio_cash_detail.yesterday_equity_sell_value
    fill_in " id", with: @iress_portfolio_cash_detail. id
    click_on "Create Iress portfolio cash detail"

    assert_text "Iress portfolio cash detail was successfully created"
    click_on "Back"
  end

  test "updating a Iress portfolio cash detail" do
    visit iress_portfolio_cash_details_url
    click_on "Edit", match: :first

    fill_in "Cash balance", with: @iress_portfolio_cash_detail.cash_balance
    fill_in "Clearing house margin", with: @iress_portfolio_cash_detail.clearing_house_margin
    fill_in "Create date time", with: @iress_portfolio_cash_detail.create_date_time
    fill_in "Created at", with: @iress_portfolio_cash_detail.created_at
    fill_in "Currency code", with: @iress_portfolio_cash_detail.currency_code
    fill_in "Deleted at", with: @iress_portfolio_cash_detail.deleted_at
    fill_in "External value", with: @iress_portfolio_cash_detail.external_value
    fill_in "Free equity", with: @iress_portfolio_cash_detail.free_equity
    fill_in "Glv", with: @iress_portfolio_cash_detail.glv
    fill_in "In market buy value", with: @iress_portfolio_cash_detail.in_market_buy_value
    fill_in "In market sell value", with: @iress_portfolio_cash_detail.in_market_sell_value
    fill_in "Margin lender total financed value", with: @iress_portfolio_cash_detail.margin_lender_total_financed_value
    fill_in "Net cash", with: @iress_portfolio_cash_detail.net_cash
    fill_in "Net unsettled buy value today", with: @iress_portfolio_cash_detail.net_unsettled_buy_value_today
    fill_in "Net unsettled sell value today", with: @iress_portfolio_cash_detail.net_unsettled_sell_value_today
    fill_in "Net unsettled value today", with: @iress_portfolio_cash_detail.net_unsettled_value_today
    fill_in "Option unsettled buy charges", with: @iress_portfolio_cash_detail.option_unsettled_buy_charges
    fill_in "Option unsettled buy value", with: @iress_portfolio_cash_detail.option_unsettled_buy_value
    fill_in "Option unsettled sell charges", with: @iress_portfolio_cash_detail.option_unsettled_sell_charges
    fill_in "Option unsettled sell value", with: @iress_portfolio_cash_detail.option_unsettled_sell_value
    fill_in "Portfolio cash code", with: @iress_portfolio_cash_detail.portfolio_cash_code
    fill_in "Portfolio cash name", with: @iress_portfolio_cash_detail.portfolio_cash_name
    fill_in "Portfolio code", with: @iress_portfolio_cash_detail.portfolio_code
    fill_in "Realized loss start of day value", with: @iress_portfolio_cash_detail.realized_loss_start_of_day_value
    fill_in "Total cfd collateral value", with: @iress_portfolio_cash_detail.total_cfd_collateral_value
    fill_in "Total cfd realised profit", with: @iress_portfolio_cash_detail.total_cfd_realised_profit
    fill_in "Total cfd realized profit in settlement currency", with: @iress_portfolio_cash_detail.total_cfd_realized_profit_in_settlement_currency
    fill_in "Total cfd unrealised profit", with: @iress_portfolio_cash_detail.total_cfd_unrealised_profit
    fill_in "Total initial margin", with: @iress_portfolio_cash_detail.total_initial_margin
    fill_in "Total non cfd market value", with: @iress_portfolio_cash_detail.total_non_cfd_market_value
    fill_in "Trust balance", with: @iress_portfolio_cash_detail.trust_balance
    fill_in "Unsettled buy charges", with: @iress_portfolio_cash_detail.unsettled_buy_charges
    fill_in "Unsettled buy value", with: @iress_portfolio_cash_detail.unsettled_buy_value
    fill_in "Unsettled sell charges", with: @iress_portfolio_cash_detail.unsettled_sell_charges
    fill_in "Unsettled sell value", with: @iress_portfolio_cash_detail.unsettled_sell_value
    fill_in "Update date time", with: @iress_portfolio_cash_detail.update_date_time
    fill_in "Updated at", with: @iress_portfolio_cash_detail.updated_at
    fill_in "Upload source", with: @iress_portfolio_cash_detail.upload_source
    fill_in "Version stamp", with: @iress_portfolio_cash_detail.version_stamp
    fill_in "Yesterday equity sell charges", with: @iress_portfolio_cash_detail.yesterday_equity_sell_charges
    fill_in "Yesterday equity sell value", with: @iress_portfolio_cash_detail.yesterday_equity_sell_value
    fill_in " id", with: @iress_portfolio_cash_detail. id
    click_on "Update Iress portfolio cash detail"

    assert_text "Iress portfolio cash detail was successfully updated"
    click_on "Back"
  end

  test "destroying a Iress portfolio cash detail" do
    visit iress_portfolio_cash_details_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Iress portfolio cash detail was successfully destroyed"
  end
end
