require "application_system_test_case"

class CashFlowTypesTest < ApplicationSystemTestCase
  setup do
    @cash_flow_type = cash_flow_types(:one)
  end

  test "visiting the index" do
    visit cash_flow_types_url
    assert_selector "h1", text: "Cash Flow Types"
  end

  test "creating a Cash flow type" do
    visit cash_flow_types_url
    click_on "New Cash Flow Type"

    fill_in "Description", with: @cash_flow_type.description
    fill_in "Id", with: @cash_flow_type.id
    click_on "Create Cash flow type"

    assert_text "Cash flow type was successfully created"
    click_on "Back"
  end

  test "updating a Cash flow type" do
    visit cash_flow_types_url
    click_on "Edit", match: :first

    fill_in "Description", with: @cash_flow_type.description
    fill_in "Id", with: @cash_flow_type.id
    click_on "Update Cash flow type"

    assert_text "Cash flow type was successfully updated"
    click_on "Back"
  end

  test "destroying a Cash flow type" do
    visit cash_flow_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Cash flow type was successfully destroyed"
  end
end
