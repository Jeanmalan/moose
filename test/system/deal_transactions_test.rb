require "application_system_test_case"

class DealTransactionsTest < ApplicationSystemTestCase
  setup do
    @deal_transaction = deal_transactions(:one)
  end

  test "visiting the index" do
    visit deal_transactions_url
    assert_selector "h1", text: "Deal Transactions"
  end

  test "creating a Deal transaction" do
    visit deal_transactions_url
    click_on "New Deal Transaction"

    fill_in "Bda branch code", with: @deal_transaction.bda_branch_code
    fill_in "Bda partner code", with: @deal_transaction.bda_partner_code
    fill_in "Cash account", with: @deal_transaction.cash_account
    fill_in "Consideration", with: @deal_transaction.consideration
    fill_in "Currency code", with: @deal_transaction.currency_code
    fill_in "Deal source", with: @deal_transaction.deal_source
    fill_in "Exchange", with: @deal_transaction.exchange
    fill_in "Fund account", with: @deal_transaction.fund_account
    fill_in "Fund name", with: @deal_transaction.fund_name
    fill_in "Industry", with: @deal_transaction.industry
    fill_in "Instrument version", with: @deal_transaction.instrument_version
    fill_in "Isin", with: @deal_transaction.isin
    fill_in "Price", with: @deal_transaction.price
    fill_in "Quantity", with: @deal_transaction.quantity
    fill_in "Reversal", with: @deal_transaction.reversal
    fill_in "Reversed transaction", with: @deal_transaction.reversed_transaction_id
    fill_in "Sector", with: @deal_transaction.sector
    fill_in "Settlement date", with: @deal_transaction.settlement_date
    fill_in "Trade date", with: @deal_transaction.trade_date
    fill_in "Transaction cost", with: @deal_transaction.transaction_cost
    fill_in "Transaction date time", with: @deal_transaction.transaction_date_time
    fill_in "Transaction", with: @deal_transaction.transaction_id
    click_on "Create Deal transaction"

    assert_text "Deal transaction was successfully created"
    click_on "Back"
  end

  test "updating a Deal transaction" do
    visit deal_transactions_url
    click_on "Edit", match: :first

    fill_in "Bda branch code", with: @deal_transaction.bda_branch_code
    fill_in "Bda partner code", with: @deal_transaction.bda_partner_code
    fill_in "Cash account", with: @deal_transaction.cash_account
    fill_in "Consideration", with: @deal_transaction.consideration
    fill_in "Currency code", with: @deal_transaction.currency_code
    fill_in "Deal source", with: @deal_transaction.deal_source
    fill_in "Exchange", with: @deal_transaction.exchange
    fill_in "Fund account", with: @deal_transaction.fund_account
    fill_in "Fund name", with: @deal_transaction.fund_name
    fill_in "Industry", with: @deal_transaction.industry
    fill_in "Instrument version", with: @deal_transaction.instrument_version
    fill_in "Isin", with: @deal_transaction.isin
    fill_in "Price", with: @deal_transaction.price
    fill_in "Quantity", with: @deal_transaction.quantity
    fill_in "Reversal", with: @deal_transaction.reversal
    fill_in "Reversed transaction", with: @deal_transaction.reversed_transaction_id
    fill_in "Sector", with: @deal_transaction.sector
    fill_in "Settlement date", with: @deal_transaction.settlement_date
    fill_in "Trade date", with: @deal_transaction.trade_date
    fill_in "Transaction cost", with: @deal_transaction.transaction_cost
    fill_in "Transaction date time", with: @deal_transaction.transaction_date_time
    fill_in "Transaction", with: @deal_transaction.transaction_id
    click_on "Update Deal transaction"

    assert_text "Deal transaction was successfully updated"
    click_on "Back"
  end

  test "destroying a Deal transaction" do
    visit deal_transactions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Deal transaction was successfully destroyed"
  end
end
