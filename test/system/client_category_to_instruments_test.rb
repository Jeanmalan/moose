require "application_system_test_case"

class ClientCategoryToInstrumentsTest < ApplicationSystemTestCase
  setup do
    @client_category_to_instrument = client_category_to_instruments(:one)
  end

  test "visiting the index" do
    visit client_category_to_instruments_url
    assert_selector "h1", text: "Client Category To Instruments"
  end

  test "creating a Client category to instrument" do
    visit client_category_to_instruments_url
    click_on "New Client Category To Instrument"

    fill_in "Client category", with: @client_category_to_instrument.client_category
    fill_in "Instrument level", with: @client_category_to_instrument.instrument_level
    fill_in "Margin rate", with: @client_category_to_instrument.margin_rate
    click_on "Create Client category to instrument"

    assert_text "Client category to instrument was successfully created"
    click_on "Back"
  end

  test "updating a Client category to instrument" do
    visit client_category_to_instruments_url
    click_on "Edit", match: :first

    fill_in "Client category", with: @client_category_to_instrument.client_category
    fill_in "Instrument level", with: @client_category_to_instrument.instrument_level
    fill_in "Margin rate", with: @client_category_to_instrument.margin_rate
    click_on "Update Client category to instrument"

    assert_text "Client category to instrument was successfully updated"
    click_on "Back"
  end

  test "destroying a Client category to instrument" do
    visit client_category_to_instruments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Client category to instrument was successfully destroyed"
  end
end
