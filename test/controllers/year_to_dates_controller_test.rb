require 'test_helper'

class YearToDatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @year_to_date = year_to_dates(:one)
  end

  test "should get index" do
    get year_to_dates_url
    assert_response :success
  end

  test "should get new" do
    get new_year_to_date_url
    assert_response :success
  end

  test "should create year_to_date" do
    assert_difference('YearToDate.count') do
      post year_to_dates_url, params: { year_to_date: { date: @year_to_date.date, fund_account: @year_to_date.fund_account, value: @year_to_date.value } }
    end

    assert_redirected_to year_to_date_url(YearToDate.last)
  end

  test "should show year_to_date" do
    get year_to_date_url(@year_to_date)
    assert_response :success
  end

  test "should get edit" do
    get edit_year_to_date_url(@year_to_date)
    assert_response :success
  end

  test "should update year_to_date" do
    patch year_to_date_url(@year_to_date), params: { year_to_date: { date: @year_to_date.date, fund_account: @year_to_date.fund_account, value: @year_to_date.value } }
    assert_redirected_to year_to_date_url(@year_to_date)
  end

  test "should destroy year_to_date" do
    assert_difference('YearToDate.count', -1) do
      delete year_to_date_url(@year_to_date)
    end

    assert_redirected_to year_to_dates_url
  end
end
