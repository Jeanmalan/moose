require 'test_helper'

class AgcDefinitionTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @agc_definition_table = agc_definition_tables(:one)
  end

  test "should get index" do
    get agc_definition_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_agc_definition_table_url
    assert_response :success
  end

  test "should create agc_definition_table" do
    assert_difference('AgcDefinitionTable.count') do
      post agc_definition_tables_url, params: { agc_definition_table: { client_name: @agc_definition_table.client_name, created_at: @agc_definition_table.created_at, deleted_at: @agc_definition_table.deleted_at, fund_account: @agc_definition_table.fund_account, fund_name: @agc_definition_table.fund_name, grouping1: @agc_definition_table.grouping1, grouping2: @agc_definition_table.grouping2, grouping3: @agc_definition_table.grouping3, grouping4: @agc_definition_table.grouping4, grouping5: @agc_definition_table.grouping5, id: @agc_definition_table.id, iress_nr: @agc_definition_table.iress_nr, strategy: @agc_definition_table.strategy, trader: @agc_definition_table.trader, updated_at: @agc_definition_table.updated_at } }
    end

    assert_redirected_to agc_definition_table_url(AgcDefinitionTable.last)
  end

  test "should show agc_definition_table" do
    get agc_definition_table_url(@agc_definition_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_agc_definition_table_url(@agc_definition_table)
    assert_response :success
  end

  test "should update agc_definition_table" do
    patch agc_definition_table_url(@agc_definition_table), params: { agc_definition_table: { client_name: @agc_definition_table.client_name, created_at: @agc_definition_table.created_at, deleted_at: @agc_definition_table.deleted_at, fund_account: @agc_definition_table.fund_account, fund_name: @agc_definition_table.fund_name, grouping1: @agc_definition_table.grouping1, grouping2: @agc_definition_table.grouping2, grouping3: @agc_definition_table.grouping3, grouping4: @agc_definition_table.grouping4, grouping5: @agc_definition_table.grouping5, id: @agc_definition_table.id, iress_nr: @agc_definition_table.iress_nr, strategy: @agc_definition_table.strategy, trader: @agc_definition_table.trader, updated_at: @agc_definition_table.updated_at } }
    assert_redirected_to agc_definition_table_url(@agc_definition_table)
  end

  test "should destroy agc_definition_table" do
    assert_difference('AgcDefinitionTable.count', -1) do
      delete agc_definition_table_url(@agc_definition_table)
    end

    assert_redirected_to agc_definition_tables_url
  end
end
