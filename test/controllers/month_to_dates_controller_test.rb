require 'test_helper'

class MonthToDatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @month_to_date = month_to_dates(:one)
  end

  test "should get index" do
    get month_to_dates_url
    assert_response :success
  end

  test "should get new" do
    get new_month_to_date_url
    assert_response :success
  end

  test "should create month_to_date" do
    assert_difference('MonthToDate.count') do
      post month_to_dates_url, params: { month_to_date: { date: @month_to_date.date, fund_account: @month_to_date.fund_account, value: @month_to_date.value } }
    end

    assert_redirected_to month_to_date_url(MonthToDate.last)
  end

  test "should show month_to_date" do
    get month_to_date_url(@month_to_date)
    assert_response :success
  end

  test "should get edit" do
    get edit_month_to_date_url(@month_to_date)
    assert_response :success
  end

  test "should update month_to_date" do
    patch month_to_date_url(@month_to_date), params: { month_to_date: { date: @month_to_date.date, fund_account: @month_to_date.fund_account, value: @month_to_date.value } }
    assert_redirected_to month_to_date_url(@month_to_date)
  end

  test "should destroy month_to_date" do
    assert_difference('MonthToDate.count', -1) do
      delete month_to_date_url(@month_to_date)
    end

    assert_redirected_to month_to_dates_url
  end
end
