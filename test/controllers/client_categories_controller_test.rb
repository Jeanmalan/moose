require 'test_helper'

class ClientCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_category = client_categories(:one)
  end

  test "should get index" do
    get client_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_client_category_url
    assert_response :success
  end

  test "should create client_category" do
    assert_difference('ClientCategory.count') do
      post client_categories_url, params: { client_category: { client_category: @client_category.client_category, fund_account: @client_category.fund_account } }
    end

    assert_redirected_to client_category_url(ClientCategory.last)
  end

  test "should show client_category" do
    get client_category_url(@client_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_category_url(@client_category)
    assert_response :success
  end

  test "should update client_category" do
    patch client_category_url(@client_category), params: { client_category: { client_category: @client_category.client_category, fund_account: @client_category.fund_account } }
    assert_redirected_to client_category_url(@client_category)
  end

  test "should destroy client_category" do
    assert_difference('ClientCategory.count', -1) do
      delete client_category_url(@client_category)
    end

    assert_redirected_to client_categories_url
  end
end
