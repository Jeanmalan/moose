require 'test_helper'

class FxRatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fx_rate = fx_rates(:one)
  end

  test "should get index" do
    get fx_rates_url
    assert_response :success
  end

  test "should get new" do
    get new_fx_rate_url
    assert_response :success
  end

  test "should create fx_rate" do
    assert_difference('FxRate.count') do
      post fx_rates_url, params: { fx_rate: { exchange_rate: @fx_rate.exchange_rate, source_currency: @fx_rate.source_currency, target_currency: @fx_rate.target_currency, valuation_date: @fx_rate.valuation_date } }
    end

    assert_redirected_to fx_rate_url(FxRate.last)
  end

  test "should show fx_rate" do
    get fx_rate_url(@fx_rate)
    assert_response :success
  end

  test "should get edit" do
    get edit_fx_rate_url(@fx_rate)
    assert_response :success
  end

  test "should update fx_rate" do
    patch fx_rate_url(@fx_rate), params: { fx_rate: { exchange_rate: @fx_rate.exchange_rate, source_currency: @fx_rate.source_currency, target_currency: @fx_rate.target_currency, valuation_date: @fx_rate.valuation_date } }
    assert_redirected_to fx_rate_url(@fx_rate)
  end

  test "should destroy fx_rate" do
    assert_difference('FxRate.count', -1) do
      delete fx_rate_url(@fx_rate)
    end

    assert_redirected_to fx_rates_url
  end
end
