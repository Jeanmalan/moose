require 'test_helper'

class IressPortfolioCashDetailsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @iress_portfolio_cash_detail = iress_portfolio_cash_details(:one)
  end

  test "should get index" do
    get iress_portfolio_cash_details_url
    assert_response :success
  end

  test "should get new" do
    get new_iress_portfolio_cash_detail_url
    assert_response :success
  end

  test "should create iress_portfolio_cash_detail" do
    assert_difference('IressPortfolioCashDetail.count') do
      post iress_portfolio_cash_details_url, params: { iress_portfolio_cash_detail: { cash_balance: @iress_portfolio_cash_detail.cash_balance, clearing_house_margin: @iress_portfolio_cash_detail.clearing_house_margin, create_date_time: @iress_portfolio_cash_detail.create_date_time, created_at: @iress_portfolio_cash_detail.created_at, currency_code: @iress_portfolio_cash_detail.currency_code, deleted_at: @iress_portfolio_cash_detail.deleted_at, external_value: @iress_portfolio_cash_detail.external_value, free_equity: @iress_portfolio_cash_detail.free_equity, glv: @iress_portfolio_cash_detail.glv, in_market_buy_value: @iress_portfolio_cash_detail.in_market_buy_value, in_market_sell_value: @iress_portfolio_cash_detail.in_market_sell_value, margin_lender_total_financed_value: @iress_portfolio_cash_detail.margin_lender_total_financed_value, net_cash: @iress_portfolio_cash_detail.net_cash, net_unsettled_buy_value_today: @iress_portfolio_cash_detail.net_unsettled_buy_value_today, net_unsettled_sell_value_today: @iress_portfolio_cash_detail.net_unsettled_sell_value_today, net_unsettled_value_today: @iress_portfolio_cash_detail.net_unsettled_value_today, option_unsettled_buy_charges: @iress_portfolio_cash_detail.option_unsettled_buy_charges, option_unsettled_buy_value: @iress_portfolio_cash_detail.option_unsettled_buy_value, option_unsettled_sell_charges: @iress_portfolio_cash_detail.option_unsettled_sell_charges, option_unsettled_sell_value: @iress_portfolio_cash_detail.option_unsettled_sell_value, portfolio_cash_code: @iress_portfolio_cash_detail.portfolio_cash_code, portfolio_cash_name: @iress_portfolio_cash_detail.portfolio_cash_name, portfolio_code: @iress_portfolio_cash_detail.portfolio_code, realized_loss_start_of_day_value: @iress_portfolio_cash_detail.realized_loss_start_of_day_value, total_cfd_collateral_value: @iress_portfolio_cash_detail.total_cfd_collateral_value, total_cfd_realised_profit: @iress_portfolio_cash_detail.total_cfd_realised_profit, total_cfd_realized_profit_in_settlement_currency: @iress_portfolio_cash_detail.total_cfd_realized_profit_in_settlement_currency, total_cfd_unrealised_profit: @iress_portfolio_cash_detail.total_cfd_unrealised_profit, total_initial_margin: @iress_portfolio_cash_detail.total_initial_margin, total_non_cfd_market_value: @iress_portfolio_cash_detail.total_non_cfd_market_value, trust_balance: @iress_portfolio_cash_detail.trust_balance, unsettled_buy_charges: @iress_portfolio_cash_detail.unsettled_buy_charges, unsettled_buy_value: @iress_portfolio_cash_detail.unsettled_buy_value, unsettled_sell_charges: @iress_portfolio_cash_detail.unsettled_sell_charges, unsettled_sell_value: @iress_portfolio_cash_detail.unsettled_sell_value, update_date_time: @iress_portfolio_cash_detail.update_date_time, updated_at: @iress_portfolio_cash_detail.updated_at, upload_source: @iress_portfolio_cash_detail.upload_source, version_stamp: @iress_portfolio_cash_detail.version_stamp, yesterday_equity_sell_charges: @iress_portfolio_cash_detail.yesterday_equity_sell_charges, yesterday_equity_sell_value: @iress_portfolio_cash_detail.yesterday_equity_sell_value,  id: @iress_portfolio_cash_detail. id } }
    end

    assert_redirected_to iress_portfolio_cash_detail_url(IressPortfolioCashDetail.last)
  end

  test "should show iress_portfolio_cash_detail" do
    get iress_portfolio_cash_detail_url(@iress_portfolio_cash_detail)
    assert_response :success
  end

  test "should get edit" do
    get edit_iress_portfolio_cash_detail_url(@iress_portfolio_cash_detail)
    assert_response :success
  end

  test "should update iress_portfolio_cash_detail" do
    patch iress_portfolio_cash_detail_url(@iress_portfolio_cash_detail), params: { iress_portfolio_cash_detail: { cash_balance: @iress_portfolio_cash_detail.cash_balance, clearing_house_margin: @iress_portfolio_cash_detail.clearing_house_margin, create_date_time: @iress_portfolio_cash_detail.create_date_time, created_at: @iress_portfolio_cash_detail.created_at, currency_code: @iress_portfolio_cash_detail.currency_code, deleted_at: @iress_portfolio_cash_detail.deleted_at, external_value: @iress_portfolio_cash_detail.external_value, free_equity: @iress_portfolio_cash_detail.free_equity, glv: @iress_portfolio_cash_detail.glv, in_market_buy_value: @iress_portfolio_cash_detail.in_market_buy_value, in_market_sell_value: @iress_portfolio_cash_detail.in_market_sell_value, margin_lender_total_financed_value: @iress_portfolio_cash_detail.margin_lender_total_financed_value, net_cash: @iress_portfolio_cash_detail.net_cash, net_unsettled_buy_value_today: @iress_portfolio_cash_detail.net_unsettled_buy_value_today, net_unsettled_sell_value_today: @iress_portfolio_cash_detail.net_unsettled_sell_value_today, net_unsettled_value_today: @iress_portfolio_cash_detail.net_unsettled_value_today, option_unsettled_buy_charges: @iress_portfolio_cash_detail.option_unsettled_buy_charges, option_unsettled_buy_value: @iress_portfolio_cash_detail.option_unsettled_buy_value, option_unsettled_sell_charges: @iress_portfolio_cash_detail.option_unsettled_sell_charges, option_unsettled_sell_value: @iress_portfolio_cash_detail.option_unsettled_sell_value, portfolio_cash_code: @iress_portfolio_cash_detail.portfolio_cash_code, portfolio_cash_name: @iress_portfolio_cash_detail.portfolio_cash_name, portfolio_code: @iress_portfolio_cash_detail.portfolio_code, realized_loss_start_of_day_value: @iress_portfolio_cash_detail.realized_loss_start_of_day_value, total_cfd_collateral_value: @iress_portfolio_cash_detail.total_cfd_collateral_value, total_cfd_realised_profit: @iress_portfolio_cash_detail.total_cfd_realised_profit, total_cfd_realized_profit_in_settlement_currency: @iress_portfolio_cash_detail.total_cfd_realized_profit_in_settlement_currency, total_cfd_unrealised_profit: @iress_portfolio_cash_detail.total_cfd_unrealised_profit, total_initial_margin: @iress_portfolio_cash_detail.total_initial_margin, total_non_cfd_market_value: @iress_portfolio_cash_detail.total_non_cfd_market_value, trust_balance: @iress_portfolio_cash_detail.trust_balance, unsettled_buy_charges: @iress_portfolio_cash_detail.unsettled_buy_charges, unsettled_buy_value: @iress_portfolio_cash_detail.unsettled_buy_value, unsettled_sell_charges: @iress_portfolio_cash_detail.unsettled_sell_charges, unsettled_sell_value: @iress_portfolio_cash_detail.unsettled_sell_value, update_date_time: @iress_portfolio_cash_detail.update_date_time, updated_at: @iress_portfolio_cash_detail.updated_at, upload_source: @iress_portfolio_cash_detail.upload_source, version_stamp: @iress_portfolio_cash_detail.version_stamp, yesterday_equity_sell_charges: @iress_portfolio_cash_detail.yesterday_equity_sell_charges, yesterday_equity_sell_value: @iress_portfolio_cash_detail.yesterday_equity_sell_value,  id: @iress_portfolio_cash_detail. id } }
    assert_redirected_to iress_portfolio_cash_detail_url(@iress_portfolio_cash_detail)
  end

  test "should destroy iress_portfolio_cash_detail" do
    assert_difference('IressPortfolioCashDetail.count', -1) do
      delete iress_portfolio_cash_detail_url(@iress_portfolio_cash_detail)
    end

    assert_redirected_to iress_portfolio_cash_details_url
  end
end
