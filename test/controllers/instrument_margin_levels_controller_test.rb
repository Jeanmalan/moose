require 'test_helper'

class InstrumentMarginLevelsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @instrument_margin_level = instrument_margin_levels(:one)
  end

  test "should get index" do
    get instrument_margin_levels_url
    assert_response :success
  end

  test "should get new" do
    get new_instrument_margin_level_url
    assert_response :success
  end

  test "should create instrument_margin_level" do
    assert_difference('InstrumentMarginLevel.count') do
      post instrument_margin_levels_url, params: { instrument_margin_level: { exchange: @instrument_margin_level.exchange, instrument_code: @instrument_margin_level.instrument_code, instrument_level: @instrument_margin_level.instrument_level } }
    end

    assert_redirected_to instrument_margin_level_url(InstrumentMarginLevel.last)
  end

  test "should show instrument_margin_level" do
    get instrument_margin_level_url(@instrument_margin_level)
    assert_response :success
  end

  test "should get edit" do
    get edit_instrument_margin_level_url(@instrument_margin_level)
    assert_response :success
  end

  test "should update instrument_margin_level" do
    patch instrument_margin_level_url(@instrument_margin_level), params: { instrument_margin_level: { exchange: @instrument_margin_level.exchange, instrument_code: @instrument_margin_level.instrument_code, instrument_level: @instrument_margin_level.instrument_level } }
    assert_redirected_to instrument_margin_level_url(@instrument_margin_level)
  end

  test "should destroy instrument_margin_level" do
    assert_difference('InstrumentMarginLevel.count', -1) do
      delete instrument_margin_level_url(@instrument_margin_level)
    end

    assert_redirected_to instrument_margin_levels_url
  end
end
