require 'test_helper'

class FinancialTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @financial_transaction = financial_transactions(:one)
  end

  test "should get index" do
    get financial_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_financial_transaction_url
    assert_response :success
  end

  test "should create financial_transaction" do
    assert_difference('FinancialTransaction.count') do
      post financial_transactions_url, params: { financial_transaction: { amount: @financial_transaction.amount, bdabranch_code: @financial_transaction.bdabranch_code, bdapartner_code: @financial_transaction.bdapartner_code, cash_account: @financial_transaction.cash_account, currency_code: @financial_transaction.currency_code, fund_account: @financial_transaction.fund_account, fund_name: @financial_transaction.fund_name, instrument_code: @financial_transaction.instrument_code, pay_date: @financial_transaction.pay_date, reversal: @financial_transaction.reversal, reversed_transaction_id: @financial_transaction.reversed_transaction_id, transaction_code: @financial_transaction.transaction_code, transaction_date_time: @financial_transaction.transaction_date_time, transaction_description: @financial_transaction.transaction_description, transaction_id: @financial_transaction.transaction_id, value_date: @financial_transaction.value_date } }
    end

    assert_redirected_to financial_transaction_url(FinancialTransaction.last)
  end

  test "should show financial_transaction" do
    get financial_transaction_url(@financial_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_financial_transaction_url(@financial_transaction)
    assert_response :success
  end

  test "should update financial_transaction" do
    patch financial_transaction_url(@financial_transaction), params: { financial_transaction: { amount: @financial_transaction.amount, bdabranch_code: @financial_transaction.bdabranch_code, bdapartner_code: @financial_transaction.bdapartner_code, cash_account: @financial_transaction.cash_account, currency_code: @financial_transaction.currency_code, fund_account: @financial_transaction.fund_account, fund_name: @financial_transaction.fund_name, instrument_code: @financial_transaction.instrument_code, pay_date: @financial_transaction.pay_date, reversal: @financial_transaction.reversal, reversed_transaction_id: @financial_transaction.reversed_transaction_id, transaction_code: @financial_transaction.transaction_code, transaction_date_time: @financial_transaction.transaction_date_time, transaction_description: @financial_transaction.transaction_description, transaction_id: @financial_transaction.transaction_id, value_date: @financial_transaction.value_date } }
    assert_redirected_to financial_transaction_url(@financial_transaction)
  end

  test "should destroy financial_transaction" do
    assert_difference('FinancialTransaction.count', -1) do
      delete financial_transaction_url(@financial_transaction)
    end

    assert_redirected_to financial_transactions_url
  end
end
