require 'test_helper'

class CashFlowSubTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cash_flow_sub_type = cash_flow_sub_types(:one)
  end

  test "should get index" do
    get cash_flow_sub_types_url
    assert_response :success
  end

  test "should get new" do
    get new_cash_flow_sub_type_url
    assert_response :success
  end

  test "should create cash_flow_sub_type" do
    assert_difference('CashFlowSubType.count') do
      post cash_flow_sub_types_url, params: { cash_flow_sub_type: { cash_flow_id: @cash_flow_sub_type.cash_flow_id, description: @cash_flow_sub_type.description } }
    end

    assert_redirected_to cash_flow_sub_type_url(CashFlowSubType.last)
  end

  test "should show cash_flow_sub_type" do
    get cash_flow_sub_type_url(@cash_flow_sub_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_cash_flow_sub_type_url(@cash_flow_sub_type)
    assert_response :success
  end

  test "should update cash_flow_sub_type" do
    patch cash_flow_sub_type_url(@cash_flow_sub_type), params: { cash_flow_sub_type: { cash_flow_id: @cash_flow_sub_type.cash_flow_id, description: @cash_flow_sub_type.description } }
    assert_redirected_to cash_flow_sub_type_url(@cash_flow_sub_type)
  end

  test "should destroy cash_flow_sub_type" do
    assert_difference('CashFlowSubType.count', -1) do
      delete cash_flow_sub_type_url(@cash_flow_sub_type)
    end

    assert_redirected_to cash_flow_sub_types_url
  end
end
