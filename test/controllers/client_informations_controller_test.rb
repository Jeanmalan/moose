require 'test_helper'

class ClientInformationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_information = client_informations(:one)
  end

  test "should get index" do
    get client_informations_url
    assert_response :success
  end

  test "should get new" do
    get new_client_information_url
    assert_response :success
  end

  test "should create client_information" do
    assert_difference('ClientInformation.count') do
      post client_informations_url, params: { client_information: { address_line1: @client_information.address_line1, address_line2: @client_information.address_line2, address_line3: @client_information.address_line3, address_line4: @client_information.address_line4, bda_branch_code: @client_information.bda_branch_code, cgt_individual: @client_information.cgt_individual, client_initials: @client_information.client_initials, client_surname: @client_information.client_surname, client_title: @client_information.client_title, email_address: @client_information.email_address, external_trading_account: @client_information.external_trading_account, first_name: @client_information.first_name, fund_account: @client_information.fund_account, fund_name: @client_information.fund_name, idno: @client_information.idno, postal: @client_information.postal, take_on_date: @client_information.take_on_date, tax_no: @client_information.tax_no, tel_no_1: @client_information.tel_no_1, tel_no_2: @client_information.tel_no_2, vat_no: @client_information.vat_no } }
    end

    assert_redirected_to client_information_url(ClientInformation.last)
  end

  test "should show client_information" do
    get client_information_url(@client_information)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_information_url(@client_information)
    assert_response :success
  end

  test "should update client_information" do
    patch client_information_url(@client_information), params: { client_information: { address_line1: @client_information.address_line1, address_line2: @client_information.address_line2, address_line3: @client_information.address_line3, address_line4: @client_information.address_line4, bda_branch_code: @client_information.bda_branch_code, cgt_individual: @client_information.cgt_individual, client_initials: @client_information.client_initials, client_surname: @client_information.client_surname, client_title: @client_information.client_title, email_address: @client_information.email_address, external_trading_account: @client_information.external_trading_account, first_name: @client_information.first_name, fund_account: @client_information.fund_account, fund_name: @client_information.fund_name, idno: @client_information.idno, postal: @client_information.postal, take_on_date: @client_information.take_on_date, tax_no: @client_information.tax_no, tel_no_1: @client_information.tel_no_1, tel_no_2: @client_information.tel_no_2, vat_no: @client_information.vat_no } }
    assert_redirected_to client_information_url(@client_information)
  end

  test "should destroy client_information" do
    assert_difference('ClientInformation.count', -1) do
      delete client_information_url(@client_information)
    end

    assert_redirected_to client_informations_url
  end
end
