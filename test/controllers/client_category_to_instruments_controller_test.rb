require 'test_helper'

class ClientCategoryToInstrumentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_category_to_instrument = client_category_to_instruments(:one)
  end

  test "should get index" do
    get client_category_to_instruments_url
    assert_response :success
  end

  test "should get new" do
    get new_client_category_to_instrument_url
    assert_response :success
  end

  test "should create client_category_to_instrument" do
    assert_difference('ClientCategoryToInstrument.count') do
      post client_category_to_instruments_url, params: { client_category_to_instrument: { client_category: @client_category_to_instrument.client_category, instrument_level: @client_category_to_instrument.instrument_level, margin_rate: @client_category_to_instrument.margin_rate } }
    end

    assert_redirected_to client_category_to_instrument_url(ClientCategoryToInstrument.last)
  end

  test "should show client_category_to_instrument" do
    get client_category_to_instrument_url(@client_category_to_instrument)
    assert_response :success
  end

  test "should get edit" do
    get edit_client_category_to_instrument_url(@client_category_to_instrument)
    assert_response :success
  end

  test "should update client_category_to_instrument" do
    patch client_category_to_instrument_url(@client_category_to_instrument), params: { client_category_to_instrument: { client_category: @client_category_to_instrument.client_category, instrument_level: @client_category_to_instrument.instrument_level, margin_rate: @client_category_to_instrument.margin_rate } }
    assert_redirected_to client_category_to_instrument_url(@client_category_to_instrument)
  end

  test "should destroy client_category_to_instrument" do
    assert_difference('ClientCategoryToInstrument.count', -1) do
      delete client_category_to_instrument_url(@client_category_to_instrument)
    end

    assert_redirected_to client_category_to_instruments_url
  end
end
