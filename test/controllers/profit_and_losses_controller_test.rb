require 'test_helper'

class ProfitAndLossesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @profit_and_loss = profit_and_losses(:one)
  end

  test "should get index" do
    get profit_and_losses_url
    assert_response :success
  end

  test "should get new" do
    get new_profit_and_loss_url
    assert_response :success
  end

  test "should create profit_and_loss" do
    assert_difference('ProfitAndLoss.count') do
      post profit_and_losses_url, params: { profit_and_loss: { closing_balance: @profit_and_loss.closing_balance, cum_percent: @profit_and_loss.cum_percent, cum_pnl: @profit_and_loss.cum_pnl, daily_percent: @profit_and_loss.daily_percent, daily_pnl: @profit_and_loss.daily_pnl, date_datetime: @profit_and_loss.date_datetime, fund_account: @profit_and_loss.fund_account, inflow: @profit_and_loss.inflow, nav: @profit_and_loss.nav, opening_balance: @profit_and_loss.opening_balance, outflow: @profit_and_loss.outflow } }
    end

    assert_redirected_to profit_and_loss_url(ProfitAndLoss.last)
  end

  test "should show profit_and_loss" do
    get profit_and_loss_url(@profit_and_loss)
    assert_response :success
  end

  test "should get edit" do
    get edit_profit_and_loss_url(@profit_and_loss)
    assert_response :success
  end

  test "should update profit_and_loss" do
    patch profit_and_loss_url(@profit_and_loss), params: { profit_and_loss: { closing_balance: @profit_and_loss.closing_balance, cum_percent: @profit_and_loss.cum_percent, cum_pnl: @profit_and_loss.cum_pnl, daily_percent: @profit_and_loss.daily_percent, daily_pnl: @profit_and_loss.daily_pnl, date_datetime: @profit_and_loss.date_datetime, fund_account: @profit_and_loss.fund_account, inflow: @profit_and_loss.inflow, nav: @profit_and_loss.nav, opening_balance: @profit_and_loss.opening_balance, outflow: @profit_and_loss.outflow } }
    assert_redirected_to profit_and_loss_url(@profit_and_loss)
  end

  test "should destroy profit_and_loss" do
    assert_difference('ProfitAndLoss.count', -1) do
      delete profit_and_loss_url(@profit_and_loss)
    end

    assert_redirected_to profit_and_losses_url
  end
end
