require 'test_helper'

class PortfolioValuationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @portfolio_valuation = portfolio_valuations(:one)
  end

  test "should get index" do
    get portfolio_valuations_url
    assert_response :success
  end

  test "should get new" do
    get new_portfolio_valuation_url
    assert_response :success
  end

  test "should create portfolio_valuation" do
    assert_difference('PortfolioValuation.count') do
      post portfolio_valuations_url, params: { portfolio_valuation: { bda_branch_code: @portfolio_valuation.bda_branch_code, bda_partner_code: @portfolio_valuation.bda_partner_code, currency_code: @portfolio_valuation.currency_code, fund_account: @portfolio_valuation.fund_account, fund_name: @portfolio_valuation.fund_name, industry: @portfolio_valuation.industry, instrument_code: @portfolio_valuation.instrument_code, instrument_code_name: @portfolio_valuation.instrument_code_name, instrument_type: @portfolio_valuation.instrument_type, instrument_version: @portfolio_valuation.instrument_version, isin: @portfolio_valuation.isin, price: @portfolio_valuation.price, quantity: @portfolio_valuation.quantity, sector: @portfolio_valuation.sector, valuation_date: @portfolio_valuation.valuation_date, wac_native: @portfolio_valuation.wac_native } }
    end

    assert_redirected_to portfolio_valuation_url(PortfolioValuation.last)
  end

  test "should show portfolio_valuation" do
    get portfolio_valuation_url(@portfolio_valuation)
    assert_response :success
  end

  test "should get edit" do
    get edit_portfolio_valuation_url(@portfolio_valuation)
    assert_response :success
  end

  test "should update portfolio_valuation" do
    patch portfolio_valuation_url(@portfolio_valuation), params: { portfolio_valuation: { bda_branch_code: @portfolio_valuation.bda_branch_code, bda_partner_code: @portfolio_valuation.bda_partner_code, currency_code: @portfolio_valuation.currency_code, fund_account: @portfolio_valuation.fund_account, fund_name: @portfolio_valuation.fund_name, industry: @portfolio_valuation.industry, instrument_code: @portfolio_valuation.instrument_code, instrument_code_name: @portfolio_valuation.instrument_code_name, instrument_type: @portfolio_valuation.instrument_type, instrument_version: @portfolio_valuation.instrument_version, isin: @portfolio_valuation.isin, price: @portfolio_valuation.price, quantity: @portfolio_valuation.quantity, sector: @portfolio_valuation.sector, valuation_date: @portfolio_valuation.valuation_date, wac_native: @portfolio_valuation.wac_native } }
    assert_redirected_to portfolio_valuation_url(@portfolio_valuation)
  end

  test "should destroy portfolio_valuation" do
    assert_difference('PortfolioValuation.count', -1) do
      delete portfolio_valuation_url(@portfolio_valuation)
    end

    assert_redirected_to portfolio_valuations_url
  end
end
