require 'test_helper'

class CashFlowTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cash_flow_type = cash_flow_types(:one)
  end

  test "should get index" do
    get cash_flow_types_url
    assert_response :success
  end

  test "should get new" do
    get new_cash_flow_type_url
    assert_response :success
  end

  test "should create cash_flow_type" do
    assert_difference('CashFlowType.count') do
      post cash_flow_types_url, params: { cash_flow_type: { description: @cash_flow_type.description, id: @cash_flow_type.id } }
    end

    assert_redirected_to cash_flow_type_url(CashFlowType.last)
  end

  test "should show cash_flow_type" do
    get cash_flow_type_url(@cash_flow_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_cash_flow_type_url(@cash_flow_type)
    assert_response :success
  end

  test "should update cash_flow_type" do
    patch cash_flow_type_url(@cash_flow_type), params: { cash_flow_type: { description: @cash_flow_type.description, id: @cash_flow_type.id } }
    assert_redirected_to cash_flow_type_url(@cash_flow_type)
  end

  test "should destroy cash_flow_type" do
    assert_difference('CashFlowType.count', -1) do
      delete cash_flow_type_url(@cash_flow_type)
    end

    assert_redirected_to cash_flow_types_url
  end
end
