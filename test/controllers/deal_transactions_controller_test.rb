require 'test_helper'

class DealTransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @deal_transaction = deal_transactions(:one)
  end

  test "should get index" do
    get deal_transactions_url
    assert_response :success
  end

  test "should get new" do
    get new_deal_transaction_url
    assert_response :success
  end

  test "should create deal_transaction" do
    assert_difference('DealTransaction.count') do
      post deal_transactions_url, params: { deal_transaction: { bda_branch_code: @deal_transaction.bda_branch_code, bda_partner_code: @deal_transaction.bda_partner_code, cash_account: @deal_transaction.cash_account, consideration: @deal_transaction.consideration, currency_code: @deal_transaction.currency_code, deal_source: @deal_transaction.deal_source, exchange: @deal_transaction.exchange, fund_account: @deal_transaction.fund_account, fund_name: @deal_transaction.fund_name, industry: @deal_transaction.industry, instrument_version: @deal_transaction.instrument_version, isin: @deal_transaction.isin, price: @deal_transaction.price, quantity: @deal_transaction.quantity, reversal: @deal_transaction.reversal, reversed_transaction_id: @deal_transaction.reversed_transaction_id, sector: @deal_transaction.sector, settlement_date: @deal_transaction.settlement_date, trade_date: @deal_transaction.trade_date, transaction_cost: @deal_transaction.transaction_cost, transaction_date_time: @deal_transaction.transaction_date_time, transaction_id: @deal_transaction.transaction_id } }
    end

    assert_redirected_to deal_transaction_url(DealTransaction.last)
  end

  test "should show deal_transaction" do
    get deal_transaction_url(@deal_transaction)
    assert_response :success
  end

  test "should get edit" do
    get edit_deal_transaction_url(@deal_transaction)
    assert_response :success
  end

  test "should update deal_transaction" do
    patch deal_transaction_url(@deal_transaction), params: { deal_transaction: { bda_branch_code: @deal_transaction.bda_branch_code, bda_partner_code: @deal_transaction.bda_partner_code, cash_account: @deal_transaction.cash_account, consideration: @deal_transaction.consideration, currency_code: @deal_transaction.currency_code, deal_source: @deal_transaction.deal_source, exchange: @deal_transaction.exchange, fund_account: @deal_transaction.fund_account, fund_name: @deal_transaction.fund_name, industry: @deal_transaction.industry, instrument_version: @deal_transaction.instrument_version, isin: @deal_transaction.isin, price: @deal_transaction.price, quantity: @deal_transaction.quantity, reversal: @deal_transaction.reversal, reversed_transaction_id: @deal_transaction.reversed_transaction_id, sector: @deal_transaction.sector, settlement_date: @deal_transaction.settlement_date, trade_date: @deal_transaction.trade_date, transaction_cost: @deal_transaction.transaction_cost, transaction_date_time: @deal_transaction.transaction_date_time, transaction_id: @deal_transaction.transaction_id } }
    assert_redirected_to deal_transaction_url(@deal_transaction)
  end

  test "should destroy deal_transaction" do
    assert_difference('DealTransaction.count', -1) do
      delete deal_transaction_url(@deal_transaction)
    end

    assert_redirected_to deal_transactions_url
  end
end
