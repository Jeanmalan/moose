/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import Vue from "vue/dist/vue.esm"
import Transactions from "../financial_transactions/transactions.vue"
import YearToDate from "../year_to_date/year_to_date.vue"
import ProfitAndLoss from "../profit_and_loss/profit_and_loss.vue"
import IressPortfolioCashDetails from "../iress/iress_portfolio_cash_details.vue"
import Profile from "../profile/profile.vue"

window.store = {}

document.addEventListener("DOMContentLoaded", () => {
  var element = document.querySelector("#transactions")
  if (element != undefined) {
    window.store.fincancetransactions = JSON.parse(element.dataset.transactions)
    window.store.fundaccounts = JSON.parse(element.dataset.fundaccounts)

    const app = new Vue({
      el: element,
      data: window.store,
      template:
        "<Transactions :original_transactions='fincancetransactions' :fundAccounts='fundaccounts' />",
      components: { Transactions }
    })
  }
})

document.addEventListener("DOMContentLoaded", () => {
  var element = document.querySelector("#yearToDate")
  if (element != undefined) {
    window.store.yeartodates = JSON.parse(element.dataset.yeartodates)
    window.store.fundaccounts = JSON.parse(element.dataset.fundaccounts)

    const app = new Vue({
      el: element,
      data: window.store,
      template:
        "<YearToDate :year_to_date='yeartodates' :fundAccounts='fundaccounts' />",
      components: { YearToDate }
    })
  }
})

document.addEventListener("DOMContentLoaded", () => {
  var element = document.querySelector("#profitandloss")
  if (element != undefined) {
    window.store.profitandloss = JSON.parse(element.dataset.profitandloss)
    window.store.fundaccounts = JSON.parse(element.dataset.fundaccounts)

    const app = new Vue({
      el: element,
      data: window.store,
      template:
        "<ProfitAndLoss :profitandloss='profitandloss' :fundAccounts='fundaccounts' />",
      components: { ProfitAndLoss }
    })
  }
})

document.addEventListener("DOMContentLoaded", () => {
  var element = document.querySelector("#iressportfoliocashdetails")
  if (element != undefined) {
    window.store.transactions = JSON.parse(element.dataset.transactions)
    console.log(window.store.transactions)

    const app = new Vue({
      el: element,
      data: window.store,
      template: "<IressPortfolioCashDetails :transactions='transactions' />",
      components: { IressPortfolioCashDetails }
    })
  }
})

document.addEventListener("DOMContentLoaded", () => {
  var element = document.querySelector("#profileDashboard")
  if (element != undefined) {
    window.store.graph_details = JSON.parse(element.dataset.graphDetails)
    window.store.portfolio_valuation = JSON.parse(
      element.dataset.portfolioValuation
    )
    window.store.portfolio_valuation_graph_detail = JSON.parse(
      element.dataset.portfolioValuationGraphDetail
    )
    window.store.profit_and_loss = JSON.parse(element.dataset.profitAndLoss)
    window.store.fund_accounts = JSON.parse(element.dataset.fundAccounts)
    window.store.month_to_dates = JSON.parse(element.dataset.monthToDates)
    const app = new Vue({
      el: element,
      data: window.store,
      template:
        "<Profile :month_to_dates='month_to_dates' :fund_accounts='fund_accounts'  :profit_and_loss='profit_and_loss' :graph_details='graph_details' :portfolio_valuation='portfolio_valuation' :portfolio_valuation_graph_detail='portfolio_valuation_graph_detail' />",
      components: { Profile }
    })
  }
})
