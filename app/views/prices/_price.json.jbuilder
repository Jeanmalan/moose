json.extract! price, :id, :exchange, :instrument_code, :currency_code, :price, :valuation_date, :created_at, :updated_at
json.url price_url(price, format: :json)
