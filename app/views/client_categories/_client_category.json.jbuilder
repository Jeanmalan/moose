json.extract! client_category, :id, :fund_account, :client_category, :created_at, :updated_at
json.url client_category_url(client_category, format: :json)
