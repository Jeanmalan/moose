json.extract! portfolio_valuation, :id, :valuation_date, :fund_account, :fund_name, :instrument_type, :instrument_code, :instrument_version, :currency_code, :quantity, :price, :wac_native, :bda_branch_code, :bda_partner_code, :instrument_code_name, :isin, :sector, :industry, :created_at, :updated_at
json.url portfolio_valuation_url(portfolio_valuation, format: :json)
