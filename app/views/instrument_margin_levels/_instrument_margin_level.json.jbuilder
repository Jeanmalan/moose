json.extract! instrument_margin_level, :id, :exchange, :instrument_code, :instrument_level, :created_at, :updated_at
json.url instrument_margin_level_url(instrument_margin_level, format: :json)
