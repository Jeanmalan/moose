json.extract! client_category_to_instrument, :id, :client_category, :instrument_level, :margin_rate, :created_at, :updated_at
json.url client_category_to_instrument_url(client_category_to_instrument, format: :json)
