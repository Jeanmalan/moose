json.extract! turnover, :id, :year, :month, :amount, :created_at, :updated_at
json.url turnover_url(turnover, format: :json)
