json.extract! month_to_date, :id, :date, :fund_account, :value, :created_at, :updated_at
json.url month_to_date_url(month_to_date, format: :json)
