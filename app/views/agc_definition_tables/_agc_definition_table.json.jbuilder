json.extract! agc_definition_table, :id, :id, :created_at, :updated_at, :deleted_at, :fund_account, :iress_nr, :client_name, :fund_name, :trader, :strategy, :grouping1, :grouping2, :grouping3, :grouping4, :grouping5, :created_at, :updated_at
json.url agc_definition_table_url(agc_definition_table, format: :json)
