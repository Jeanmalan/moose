json.extract! profit_and_loss, :id, :date_datetime, :fund_account, :inflow, :outflow, :nav, :opening_balance, :closing_balance, :daily_pnl, :cum_pnl, :daily_percent, :cum_percent, :created_at, :updated_at
json.url profit_and_loss_url(profit_and_loss, format: :json)
