json.extract! instrument, :id, :valuation_date, :exchange, :instrument_code, :currency_code, :isin, :sedol, :instrument_name, :sector, :industry, :created_at, :updated_at
json.url instrument_url(instrument, format: :json)
