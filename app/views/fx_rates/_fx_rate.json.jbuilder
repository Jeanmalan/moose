json.extract! fx_rate, :id, :valuation_date, :source_currency, :target_currency, :exchange_rate, :created_at, :updated_at
json.url fx_rate_url(fx_rate, format: :json)
