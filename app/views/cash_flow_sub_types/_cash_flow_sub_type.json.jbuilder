json.extract! cash_flow_sub_type, :id, :cash_flow_id, :description, :created_at, :updated_at
json.url cash_flow_sub_type_url(cash_flow_sub_type, format: :json)
