json.extract! year_to_date, :id, :date, :fund_account, :value, :created_at, :updated_at
json.url year_to_date_url(year_to_date, format: :json)
