class AgcDefinitionTablesController < ApplicationController
  before_action :set_agc_definition_table, only: [:show, :edit, :update, :destroy]

  # GET /agc_definition_tables
  # GET /agc_definition_tables.json
  def index
    @agc_definition_tables = AgcDefinitionTable.all
  end

  # GET /agc_definition_tables/1
  # GET /agc_definition_tables/1.json
  def show
  end

  # GET /agc_definition_tables/new
  def new
    @agc_definition_table = AgcDefinitionTable.new
  end

  # GET /agc_definition_tables/1/edit
  def edit
  end

  # POST /agc_definition_tables
  # POST /agc_definition_tables.json
  def create
    @agc_definition_table = AgcDefinitionTable.new(agc_definition_table_params)

    respond_to do |format|
      if @agc_definition_table.save
        format.html { redirect_to @agc_definition_table, notice: 'Agc definition table was successfully created.' }
        format.json { render :show, status: :created, location: @agc_definition_table }
      else
        format.html { render :new }
        format.json { render json: @agc_definition_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /agc_definition_tables/1
  # PATCH/PUT /agc_definition_tables/1.json
  def update
    respond_to do |format|
      if @agc_definition_table.update(agc_definition_table_params)
        format.html { redirect_to @agc_definition_table, notice: 'Agc definition table was successfully updated.' }
        format.json { render :show, status: :ok, location: @agc_definition_table }
      else
        format.html { render :edit }
        format.json { render json: @agc_definition_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /agc_definition_tables/1
  # DELETE /agc_definition_tables/1.json
  def destroy
    @agc_definition_table.destroy
    respond_to do |format|
      format.html { redirect_to agc_definition_tables_url, notice: 'Agc definition table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_agc_definition_table
      @agc_definition_table = AgcDefinitionTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def agc_definition_table_params
      params.require(:agc_definition_table).permit(:id, :created_at, :updated_at, :deleted_at, :fund_account, :iress_nr, :client_name, :fund_name, :trader, :strategy, :grouping1, :grouping2, :grouping3, :grouping4, :grouping5)
    end
end
