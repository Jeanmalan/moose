class CashFlowTypesController < ApplicationController
  before_action :set_cash_flow_type, only: [:show, :edit, :update, :destroy]

  # GET /cash_flow_types
  # GET /cash_flow_types.json
  def index
    @cash_flow_types = CashFlowType.all
  end

  # GET /cash_flow_types/1
  # GET /cash_flow_types/1.json
  def show
  end

  # GET /cash_flow_types/new
  def new
    @cash_flow_type = CashFlowType.new
  end

  # GET /cash_flow_types/1/edit
  def edit
  end

  # POST /cash_flow_types
  # POST /cash_flow_types.json
  def create
    @cash_flow_type = CashFlowType.new(cash_flow_type_params)

    respond_to do |format|
      if @cash_flow_type.save
        format.html { redirect_to @cash_flow_type, notice: 'Cash flow type was successfully created.' }
        format.json { render :show, status: :created, location: @cash_flow_type }
      else
        format.html { render :new }
        format.json { render json: @cash_flow_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_flow_types/1
  # PATCH/PUT /cash_flow_types/1.json
  def update
    respond_to do |format|
      if @cash_flow_type.update(cash_flow_type_params)
        format.html { redirect_to @cash_flow_type, notice: 'Cash flow type was successfully updated.' }
        format.json { render :show, status: :ok, location: @cash_flow_type }
      else
        format.html { render :edit }
        format.json { render json: @cash_flow_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_flow_types/1
  # DELETE /cash_flow_types/1.json
  def destroy
    @cash_flow_type.destroy
    respond_to do |format|
      format.html { redirect_to cash_flow_types_url, notice: 'Cash flow type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_flow_type
      @cash_flow_type = CashFlowType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_flow_type_params
      params.require(:cash_flow_type).permit(:description, :id)
    end
end
