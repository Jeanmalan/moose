class InstrumentMarginLevelsController < ApplicationController
  before_action :set_instrument_margin_level, only: [:show, :edit, :update, :destroy]

  # GET /instrument_margin_levels
  # GET /instrument_margin_levels.json
  def index
    @instrument_margin_levels = InstrumentMarginLevel.all
  end

  # GET /instrument_margin_levels/1
  # GET /instrument_margin_levels/1.json
  def show
  end

  # GET /instrument_margin_levels/new
  def new
    @instrument_margin_level = InstrumentMarginLevel.new
  end

  # GET /instrument_margin_levels/1/edit
  def edit
  end

  # POST /instrument_margin_levels
  # POST /instrument_margin_levels.json
  def create
    @instrument_margin_level = InstrumentMarginLevel.new(instrument_margin_level_params)

    respond_to do |format|
      if @instrument_margin_level.save
        format.html { redirect_to @instrument_margin_level, notice: 'Instrument margin level was successfully created.' }
        format.json { render :show, status: :created, location: @instrument_margin_level }
      else
        format.html { render :new }
        format.json { render json: @instrument_margin_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instrument_margin_levels/1
  # PATCH/PUT /instrument_margin_levels/1.json
  def update
    respond_to do |format|
      if @instrument_margin_level.update(instrument_margin_level_params)
        format.html { redirect_to @instrument_margin_level, notice: 'Instrument margin level was successfully updated.' }
        format.json { render :show, status: :ok, location: @instrument_margin_level }
      else
        format.html { render :edit }
        format.json { render json: @instrument_margin_level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /instrument_margin_levels/1
  # DELETE /instrument_margin_levels/1.json
  def destroy
    @instrument_margin_level.destroy
    respond_to do |format|
      format.html { redirect_to instrument_margin_levels_url, notice: 'Instrument margin level was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instrument_margin_level
      @instrument_margin_level = InstrumentMarginLevel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def instrument_margin_level_params
      params.require(:instrument_margin_level).permit(:exchange, :instrument_code, :instrument_level)
    end
end
