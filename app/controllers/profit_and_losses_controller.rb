class ProfitAndLossesController < ApplicationController
  before_action :set_profit_and_loss, only: [:show, :edit, :update, :destroy, ]
  before_action :authenticate_user!
  respond_to :json

  # GET /profit_and_losses
  # GET /profit_and_losses.json
  def index
    @profit_and_losses = ProfitAndLoss.all.where('fund_account =?', current_user.fund_account)
    accounts = []
    # @profit_and_losses.map{|x| accounts.push(x.fund_account)}
    @profit_and_losses.map { |pnl| accounts << {'name' => pnl.fund_account, 'value' => pnl.fund_account} }

    @profit_and_losses.map{|x|  x.closing_balance = x.nav.round(2)
                                x.closing_balance = x.closing_balance.round(2)
                                x.opening_balance = x.opening_balance.round(2)
                                x.daily_pnl = x.daily_pnl.round(2)
                                x.cum_pnl = x.cum_pnl.round(2)
                                x.cum_percent = x.cum_percent.round(2)
                                x.daily_percent = x.daily_percent.round(2) }
  
    @fund_accounts = accounts.uniq! {|a| a['value'] }
  end


  def get_profit_and_loss_by_fund_account
    @data = []
    # 1- Get all profit and loss for account
    fund_account = profit_and_loss_params[:fund_account]
    profit_and_losses = ProfitAndLoss.get_profit_and_loss_by_account(fund_account)
    # 2- Get all grapgh data for account
    graph_details = {'list_date': [], 'list_daily_pnls': [], 'colours': [], 'list_pnl_ids': []}
    #get coloor for graph depending on integer
    profit_and_losses.select{|x| x.daily_pnl > 0 ? graph_details[:colours] << '#21ce99' : graph_details[:colours] <<'#E6512F'}
    profit_and_losses.select{|x|  graph_details[:list_date] << x.date.to_s[0,11]
                                  graph_details[:list_daily_pnls] << x.daily_pnl
                                  graph_details[:list_pnl_ids]  << x.daily_pnl}

    @data.push(graph_details)
    @data.push(profit_and_losses.order('date DESC'))

    respond_to do |format|
      format.json { render :json => @data.to_json }  
    end
  end

  # GET /profit_and_losses/1
  # GET /profit_and_losses/1.json
  def show
  end

  # GET /profit_and_losses/new
  def new
    @profit_and_loss = ProfitAndLoss.new
  end

  # GET /profit_and_losses/1/edit
  def edit
  end

  # POST /profit_and_losses
  # POST /profit_and_losses.json
  def create
    @profit_and_loss = ProfitAndLoss.new(profit_and_loss_params)

    respond_to do |format|
      if @profit_and_loss.save
        format.html { redirect_to @profit_and_loss, notice: 'Profit and loss was successfully created.' }
        format.json { render :show, status: :created, location: @profit_and_loss }
      else
        format.html { render :new }
        format.json { render json: @profit_and_loss.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profit_and_losses/1
  # PATCH/PUT /profit_and_losses/1.json
  def update
    respond_to do |format|
      if @profit_and_loss.update(profit_and_loss_params)
        format.html { redirect_to @profit_and_loss, notice: 'Profit and loss was successfully updated.' }
        format.json { render :show, status: :ok, location: @profit_and_loss }
      else
        format.html { render :edit }
        format.json { render json: @profit_and_loss.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profit_and_losses/1
  # DELETE /profit_and_losses/1.json
  def destroy
    @profit_and_loss.destroy
    respond_to do |format|
      format.html { redirect_to profit_and_losses_url, notice: 'Profit and loss was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profit_and_loss
      @profit_and_loss = ProfitAndLoss.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profit_and_loss_params
      params.require(:profit_and_loss).permit(:date_datetime, :fund_account, :inflow, :outflow, :nav, :opening_balance, :closing_balance, :daily_pnl, :cum_pnl, :daily_percent, :cum_percent)
    end
end
