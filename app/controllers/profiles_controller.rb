class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /profiles
  # GET /profiles.json
  def index

    @graph_details = {'list_date': [], 'list_daily_pnls': [], 'colours': [], 'list_pnl_ids': []}
    @user_fund_accounts = Profile.get_user_fund_accounts(current_user.id)

    # get all profit and loss by sending through User ID
    pnl = Profile.get_profit_and_loss_by_user_id(current_user.id)

    #collect array of colors repsonding to daily pnl
    pnl.select{|x| x.daily_pnl > 0 ? @graph_details[:colours] << '#21ce99' : @graph_details[:colours] <<'#E6512F'}

    #collect an array for graph details
    pnl.select{|x|  @graph_details[:list_date] << x.date.to_s[0,11]
    @graph_details[:list_daily_pnls] << x.daily_pnl
    @graph_details[:list_pnl_ids]  << x.daily_pnl}
    @month_to_dates = MonthToDate.get_all_month_to_date(@user_fund_accounts)

  
  @portfolio_valuations = PortfolioValuation.get_portfolio_valuation_by_account(@user_fund_accounts)
  @portfolio_valuation_graph_detail = PortfolioValuation.get_portfolio_valuation_graph_detail(@user_fund_accounts)

  @get_profit_and_loss_by_account = ProfitAndLoss.get_profit_and_loss_by_account(@user_fund_accounts)

  end

  def get_profit_and_loss 

    @profit_and_losses = ProfitAndLoss.all
                                      .where('fund_account =?', current_user.fund_account)
                                      .map{|x|  x.closing_balance = x.nav.round(2)
                                                x.closing_balance = x.closing_balance.round(2)
                                                x.opening_balance = x.opening_balance.round(2)
                                                x.daily_pnl = x.daily_pnl.round(2)
                                                x.cum_pnl = x.cum_pnl.round(2)
                                                x.cum_percent = x.cum_percent.round(2)
                                                x.daily_percent = x.daily_percent.round(2) }
                                               
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.fetch(:profile, {})
    end
end
