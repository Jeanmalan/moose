class ClientCategoryToInstrumentsController < ApplicationController
  before_action :set_client_category_to_instrument, only: [:show, :edit, :update, :destroy]

  # GET /client_category_to_instruments
  # GET /client_category_to_instruments.json
  def index
    @client_category_to_instruments = ClientCategoryToInstrument.all
  end

  # GET /client_category_to_instruments/1
  # GET /client_category_to_instruments/1.json
  def show
  end

  # GET /client_category_to_instruments/new
  def new
    @client_category_to_instrument = ClientCategoryToInstrument.new
  end

  # GET /client_category_to_instruments/1/edit
  def edit
  end

  # POST /client_category_to_instruments
  # POST /client_category_to_instruments.json
  def create
    @client_category_to_instrument = ClientCategoryToInstrument.new(client_category_to_instrument_params)

    respond_to do |format|
      if @client_category_to_instrument.save
        format.html { redirect_to @client_category_to_instrument, notice: 'Client category to instrument was successfully created.' }
        format.json { render :show, status: :created, location: @client_category_to_instrument }
      else
        format.html { render :new }
        format.json { render json: @client_category_to_instrument.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /client_category_to_instruments/1
  # PATCH/PUT /client_category_to_instruments/1.json
  def update
    respond_to do |format|
      if @client_category_to_instrument.update(client_category_to_instrument_params)
        format.html { redirect_to @client_category_to_instrument, notice: 'Client category to instrument was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_category_to_instrument }
      else
        format.html { render :edit }
        format.json { render json: @client_category_to_instrument.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_category_to_instruments/1
  # DELETE /client_category_to_instruments/1.json
  def destroy
    @client_category_to_instrument.destroy
    respond_to do |format|
      format.html { redirect_to client_category_to_instruments_url, notice: 'Client category to instrument was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_category_to_instrument
      @client_category_to_instrument = ClientCategoryToInstrument.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_category_to_instrument_params
      params.require(:client_category_to_instrument).permit(:client_category, :instrument_level, :margin_rate)
    end
end
