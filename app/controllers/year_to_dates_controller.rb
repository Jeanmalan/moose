class YearToDatesController < ApplicationController
  before_action :set_year_to_date, only: [:show, :edit, :update, :destroy]
  attr_accessor :value2

  # GET /year_to_dates
  # GET /year_to_dates.json
  def index
    @year_to_dates = YearToDate.last(2000)
    accounts = []
    @year_to_dates.map{|ft| accounts.push(ft.fund_account)}
    @fund_accounts = accounts.uniq

  end

  # GET /year_to_dates/1
  # GET /year_to_dates/1.json
  def show
  end

  # GET /year_to_dates/new
  def new
    @year_to_date = YearToDate.new
  end

  # GET /year_to_dates/1/edit
  def edit
  end

  # POST /year_to_dates
  # POST /year_to_dates.json
  def create
    @year_to_date = YearToDate.new(year_to_date_params)

    respond_to do |format|
      if @year_to_date.save
        format.html { redirect_to @year_to_date, notice: 'Year to date was successfully created.' }
        format.json { render :show, status: :created, location: @year_to_date }
      else
        format.html { render :new }
        format.json { render json: @year_to_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /year_to_dates/1
  # PATCH/PUT /year_to_dates/1.json
  def update
    respond_to do |format|
      if @year_to_date.update(year_to_date_params)
        format.html { redirect_to @year_to_date, notice: 'Year to date was successfully updated.' }
        format.json { render :show, status: :ok, location: @year_to_date }
      else
        format.html { render :edit }
        format.json { render json: @year_to_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /year_to_dates/1
  # DELETE /year_to_dates/1.json
  def destroy
    @year_to_date.destroy
    respond_to do |format|
      format.html { redirect_to year_to_dates_url, notice: 'Year to date was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_year_to_date
      @year_to_date = YearToDate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def year_to_date_params
      params.require(:year_to_date).permit(:date, :fund_account, :value)
    end
end
