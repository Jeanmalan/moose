class IressPortfolioCashDetailsController < ApplicationController
  before_action :set_iress_portfolio_cash_detail, only: [:show, :edit, :update, :destroy]

  # GET /iress_portfolio_cash_details
  # GET /iress_portfolio_cash_details.json
  def index
    @iress_portfolio_cash_details = IressPortfolioCashDetail.all
  end

  # GET /iress_portfolio_cash_details/1
  # GET /iress_portfolio_cash_details/1.json
  def show
  end

  # GET /iress_portfolio_cash_details/new
  def new
    @iress_portfolio_cash_detail = IressPortfolioCashDetail.new
  end

  # GET /iress_portfolio_cash_details/1/edit
  def edit
  end

  # POST /iress_portfolio_cash_details
  # POST /iress_portfolio_cash_details.json
  def create
    @iress_portfolio_cash_detail = IressPortfolioCashDetail.new(iress_portfolio_cash_detail_params)

    respond_to do |format|
      if @iress_portfolio_cash_detail.save
        format.html { redirect_to @iress_portfolio_cash_detail, notice: 'Iress portfolio cash detail was successfully created.' }
        format.json { render :show, status: :created, location: @iress_portfolio_cash_detail }
      else
        format.html { render :new }
        format.json { render json: @iress_portfolio_cash_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /iress_portfolio_cash_details/1
  # PATCH/PUT /iress_portfolio_cash_details/1.json
  def update
    respond_to do |format|
      if @iress_portfolio_cash_detail.update(iress_portfolio_cash_detail_params)
        format.html { redirect_to @iress_portfolio_cash_detail, notice: 'Iress portfolio cash detail was successfully updated.' }
        format.json { render :show, status: :ok, location: @iress_portfolio_cash_detail }
      else
        format.html { render :edit }
        format.json { render json: @iress_portfolio_cash_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /iress_portfolio_cash_details/1
  # DELETE /iress_portfolio_cash_details/1.json
  def destroy
    @iress_portfolio_cash_detail.destroy
    respond_to do |format|
      format.html { redirect_to iress_portfolio_cash_details_url, notice: 'Iress portfolio cash detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_iress_portfolio_cash_detail
      @iress_portfolio_cash_detail = IressPortfolioCashDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def iress_portfolio_cash_detail_params
      params.require(:iress_portfolio_cash_detail).permit(: id, :created_at, :updated_at, :deleted_at, :portfolio_code, :portfolio_cash_code, :portfolio_cash_name, :version_stamp, :create_date_time, :update_date_time, :currency_code, :cash_balance, :unsettled_buy_value, :unsettled_buy_charges, :unsettled_sell_value, :unsettled_sell_charges, :yesterday_equity_sell_value, :yesterday_equity_sell_charges, :in_market_buy_value, :in_market_sell_value, :net_cash, :upload_source, :option_unsettled_buy_value, :option_unsettled_buy_charges, :option_unsettled_sell_value, :option_unsettled_sell_charges, :clearing_house_margin, :external_value, :net_unsettled_buy_value_today, :net_unsettled_sell_value_today, :net_unsettled_value_today, :glv, :free_equity, :total_initial_margin, :total_cfd_realised_profit, :total_cfd_unrealised_profit, :total_cfd_collateral_value, :total_non_cfd_market_value, :realized_loss_start_of_day_value, :margin_lender_total_financed_value, :trust_balance, :total_cfd_realized_profit_in_settlement_currency)
    end
end
