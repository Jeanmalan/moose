class FinancialTransactionsController < ApplicationController
  before_action :set_financial_transaction, only: [:show, :edit, :update, :destroy]
  respond_to :json

  # GET /financial_transactions
  # GET /financial_transactions.json
  def index
    @financial_transactions = FinancialTransaction.last(200)
    accounts = []
    @financial_transactions.map{|ft| accounts.push(ft.fund_account)}
    @fund_accounts = accounts.uniq
    # unique_fund_accounts = accounts.uniq
    # @fund_accounts = []
    # unique_fund_accounts.each{|f| @fund_accounts << {"id" => f, "name" => f} }
  end

  # GET /financial_transactions/1
  # GET /financial_transactions/1.json
  def show
  end

  def get_ft_by_fund_account_and_date
    fund_account = financial_transaction_params[:fund_account]
    value_date = financial_transaction_params[:value_date].to_datetime

    @ft = FinancialTransaction.get_transactions_by_date_and_account(fund_account, value_date)

    respond_to do |format|
      format.json { render :json => @ft.to_json }  
    end

  end

  # GET /financial_transactions/new
  def new
    @financial_transaction = FinancialTransaction.new
  end

  # GET /financial_transactions/1/edit
  def edit
  end

  # POST /financial_transactions
  # POST /financial_transactions.json
  def create
    @financial_transaction = FinancialTransaction.new(financial_transaction_params)

    respond_to do |format|
      if @financial_transaction.save
        format.html { redirect_to @financial_transaction, notice: 'Financial transaction was successfully created.' }
        format.json { render :show, status: :created, location: @financial_transaction }
      else
        format.html { render :new }
        format.json { render json: @financial_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /financial_transactions/1
  # PATCH/PUT /financial_transactions/1.json
  def update
    respond_to do |format|
      if @financial_transaction.update(financial_transaction_params)
        format.html { redirect_to @financial_transaction, notice: 'Financial transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @financial_transaction }
      else
        format.html { render :edit }
        format.json { render json: @financial_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /financial_transactions/1
  # DELETE /financial_transactions/1.json
  def destroy
    @financial_transaction.destroy
    respond_to do |format|
      format.html { redirect_to financial_transactions_url, notice: 'Financial transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_financial_transaction
      @financial_transaction = FinancialTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def financial_transaction_params
      params.require(:financial_transaction).permit(:transaction_id, :fund_account, :fund_name, :transaction_date_time, :transaction_code, :currency_code, :cash_account, :value_date, :pay_date, :amount, :transaction_description, :reversal, :reversed_transaction_id, :bdabranch_code, :bdapartner_code, :instrument_code)
    end
end
