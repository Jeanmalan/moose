class PortfolioValuationsController < ApplicationController
  before_action :set_portfolio_valuation, only: [:show, :edit, :update, :destroy]
  respond_to :json


  # GET /portfolio_valuations
  # GET /portfolio_valuations.json
  def index
    @portfolio_valuations = PortfolioValuation.all
  end

  #returns all data for portfolio valuation graph
  def get_report_graph_portfolio_valuations
    @data = [] 
    fund_account = []
    fund_account.push(portfolio_valuation_params[:fund_account])
    instrument_code_name = portfolio_valuation_params[:instrument_code_name]
    
    # Get All User fund Accounts
    @user_fund_accounts = Profile.get_user_fund_accounts(current_user.id)
    @portfolio_by_account = PortfolioValuation.get_portfolio_valuation_by_account(fund_account)
    @get_portfolio_valuation_graph_unique_detail = PortfolioValuation.get_portfolio_valuation_graph_unique_detail(fund_account, instrument_code_name)
    @data.push(@get_portfolio_valuation_graph_unique_detail)

    # respond_with @data.to_json
    respond_to do |format|
      format.json { render :json => @data[0] }  
    end
  end

  def get_portfoilio_valuation_details

    
    fund_account = portfolio_valuation_params[:fund_account]
    @portfolio_valuation = PortfolioValuation.portfolio_valuation_by_fund_account_number(current_user.id, fund_account)
    respond_to do |format|
      format.json { render :json => @portfolio_valuation }  
    end
  end

  # GET /portfolio_valuations/1
  # GET /portfolio_valuations/1.json
  def show
  end

  # GET /portfolio_valuations/new
  def new
    @portfolio_valuation = PortfolioValuation.new
  end

  # GET /portfolio_valuations/1/edit
  def edit
  end

  # POST /portfolio_valuations
  # POST /portfolio_valuations.json
  def create
    @portfolio_valuation = PortfolioValuation.new(portfolio_valuation_params)

    respond_to do |format|
      if @portfolio_valuation.save
        format.html { redirect_to @portfolio_valuation, notice: 'Portfolio valuation was successfully created.' }
        format.json { render :show, status: :created, location: @portfolio_valuation }
      else
        format.html { render :new }
        format.json { render json: @portfolio_valuation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /portfolio_valuations/1
  # PATCH/PUT /portfolio_valuations/1.json
  def update
    respond_to do |format|
      if @portfolio_valuation.update(portfolio_valuation_params)
        format.html { redirect_to @portfolio_valuation, notice: 'Portfolio valuation was successfully updated.' }
        format.json { render :show, status: :ok, location: @portfolio_valuation }
      else
        format.html { render :edit }
        format.json { render json: @portfolio_valuation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /portfolio_valuations/1
  # DELETE /portfolio_valuations/1.json
  def destroy
    @portfolio_valuation.destroy
    respond_to do |format|
      format.html { redirect_to portfolio_valuations_url, notice: 'Portfolio valuation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_portfolio_valuation
      @portfolio_valuation = PortfolioValuation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def portfolio_valuation_params
      params.require(:portfolio_valuation).permit(:formData, :valuation_date, :fund_account, :fund_name, :instrument_type, :instrument_code, :instrument_version, :currency_code, :quantity, :price, :wac_native, :bda_branch_code, :bda_partner_code, :instrument_code_name, :isin, :sector, :industry)
    end
end
