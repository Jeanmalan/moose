class CashFlowSubTypesController < ApplicationController
  before_action :set_cash_flow_sub_type, only: [:show, :edit, :update, :destroy]

  # GET /cash_flow_sub_types
  # GET /cash_flow_sub_types.json
  def index
    @cash_flow_sub_types = CashFlowSubType.all
  end

  # GET /cash_flow_sub_types/1
  # GET /cash_flow_sub_types/1.json
  def show
  end

  # GET /cash_flow_sub_types/new
  def new
    @cash_flow_sub_type = CashFlowSubType.new
  end

  # GET /cash_flow_sub_types/1/edit
  def edit
  end

  # POST /cash_flow_sub_types
  # POST /cash_flow_sub_types.json
  def create
    @cash_flow_sub_type = CashFlowSubType.new(cash_flow_sub_type_params)

    respond_to do |format|
      if @cash_flow_sub_type.save
        format.html { redirect_to @cash_flow_sub_type, notice: 'Cash flow sub type was successfully created.' }
        format.json { render :show, status: :created, location: @cash_flow_sub_type }
      else
        format.html { render :new }
        format.json { render json: @cash_flow_sub_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cash_flow_sub_types/1
  # PATCH/PUT /cash_flow_sub_types/1.json
  def update
    respond_to do |format|
      if @cash_flow_sub_type.update(cash_flow_sub_type_params)
        format.html { redirect_to @cash_flow_sub_type, notice: 'Cash flow sub type was successfully updated.' }
        format.json { render :show, status: :ok, location: @cash_flow_sub_type }
      else
        format.html { render :edit }
        format.json { render json: @cash_flow_sub_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cash_flow_sub_types/1
  # DELETE /cash_flow_sub_types/1.json
  def destroy
    @cash_flow_sub_type.destroy
    respond_to do |format|
      format.html { redirect_to cash_flow_sub_types_url, notice: 'Cash flow sub type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cash_flow_sub_type
      @cash_flow_sub_type = CashFlowSubType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cash_flow_sub_type_params
      params.require(:cash_flow_sub_type).permit(:cash_flow_id, :description)
    end
end
