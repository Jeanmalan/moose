class ClientInformationsController < ApplicationController
  before_action :set_client_information, only: [:show, :edit, :update, :destroy]

  # GET /client_informations
  # GET /client_informations.json
  def index
    @client_informations = ClientInformation.all
  end

  # GET /client_informations/1
  # GET /client_informations/1.json
  def show
  end

  # GET /client_informations/new
  def new
    @client_information = ClientInformation.new
  end

  # GET /client_informations/1/edit
  def edit
  end

  # POST /client_informations
  # POST /client_informations.json
  def create
    @client_information = ClientInformation.new(client_information_params)

    respond_to do |format|
      if @client_information.save
        format.html { redirect_to @client_information, notice: 'Client information was successfully created.' }
        format.json { render :show, status: :created, location: @client_information }
      else
        format.html { render :new }
        format.json { render json: @client_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /client_informations/1
  # PATCH/PUT /client_informations/1.json
  def update
    respond_to do |format|
      if @client_information.update(client_information_params)
        format.html { redirect_to @client_information, notice: 'Client information was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_information }
      else
        format.html { render :edit }
        format.json { render json: @client_information.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_informations/1
  # DELETE /client_informations/1.json
  def destroy
    @client_information.destroy
    respond_to do |format|
      format.html { redirect_to client_informations_url, notice: 'Client information was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_information
      @client_information = ClientInformation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_information_params
      params.require(:client_information).permit(:fund_account, :fund_name, :bda_branch_code, :take_on_date, :client_title, :client_initials, :first_name, :client_surname, :idno, :tel_no_1, :tel_no_2, :email_address, :tax_no, :vat_no, :address_line1, :address_line2, :address_line3, :address_line4, :postal, :external_trading_account, :cgt_individual)
    end
end
