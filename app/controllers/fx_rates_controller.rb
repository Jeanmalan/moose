class FxRatesController < ApplicationController
  before_action :set_fx_rate, only: [:show, :edit, :update, :destroy]

  # GET /fx_rates
  # GET /fx_rates.json
  def index
    @fx_rates = FxRate.all
  end

  # GET /fx_rates/1
  # GET /fx_rates/1.json
  def show
  end

  # GET /fx_rates/new
  def new
    @fx_rate = FxRate.new
  end

  # GET /fx_rates/1/edit
  def edit
  end

  # POST /fx_rates
  # POST /fx_rates.json
  def create
    @fx_rate = FxRate.new(fx_rate_params)

    respond_to do |format|
      if @fx_rate.save
        format.html { redirect_to @fx_rate, notice: 'Fx rate was successfully created.' }
        format.json { render :show, status: :created, location: @fx_rate }
      else
        format.html { render :new }
        format.json { render json: @fx_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fx_rates/1
  # PATCH/PUT /fx_rates/1.json
  def update
    respond_to do |format|
      if @fx_rate.update(fx_rate_params)
        format.html { redirect_to @fx_rate, notice: 'Fx rate was successfully updated.' }
        format.json { render :show, status: :ok, location: @fx_rate }
      else
        format.html { render :edit }
        format.json { render json: @fx_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fx_rates/1
  # DELETE /fx_rates/1.json
  def destroy
    @fx_rate.destroy
    respond_to do |format|
      format.html { redirect_to fx_rates_url, notice: 'Fx rate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fx_rate
      @fx_rate = FxRate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fx_rate_params
      params.require(:fx_rate).permit(:valuation_date, :source_currency, :target_currency, :exchange_rate)
    end
end
