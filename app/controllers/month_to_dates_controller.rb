class MonthToDatesController < ApplicationController
  before_action :set_month_to_date, only: [:show, :edit, :update, :destroy]

  # GET /month_to_dates
  # GET /month_to_dates.json
  def index
    @month_to_dates = MonthToDate.get_all_month_to_date
  end

  # GET /month_to_dates/1
  # GET /month_to_dates/1.json
  def show
  end

  # GET /month_to_dates/new
  def new
    @month_to_date = MonthToDate.new
  end

  # GET /month_to_dates/1/edit
  def edit
  end

  # POST /month_to_dates
  # POST /month_to_dates.json
  def create
    @month_to_date = MonthToDate.new(month_to_date_params)

    respond_to do |format|
      if @month_to_date.save
        format.html { redirect_to @month_to_date, notice: 'Month to date was successfully created.' }
        format.json { render :show, status: :created, location: @month_to_date }
      else
        format.html { render :new }
        format.json { render json: @month_to_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /month_to_dates/1
  # PATCH/PUT /month_to_dates/1.json
  def update
    respond_to do |format|
      if @month_to_date.update(month_to_date_params)
        format.html { redirect_to @month_to_date, notice: 'Month to date was successfully updated.' }
        format.json { render :show, status: :ok, location: @month_to_date }
      else
        format.html { render :edit }
        format.json { render json: @month_to_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /month_to_dates/1
  # DELETE /month_to_dates/1.json
  def destroy
    @month_to_date.destroy
    respond_to do |format|
      format.html { redirect_to month_to_dates_url, notice: 'Month to date was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_month_to_date
      @month_to_date = MonthToDate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def month_to_date_params
      params.require(:month_to_date).permit(:date, :fund_account, :value)
    end
end
