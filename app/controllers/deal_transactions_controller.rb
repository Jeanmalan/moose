class DealTransactionsController < ApplicationController
  before_action :set_deal_transaction, only: [:show, :edit, :update, :destroy]

  # GET /deal_transactions
  # GET /deal_transactions.json
  def index
    @deal_transactions = DealTransaction.all
  end

  # GET /deal_transactions/1
  # GET /deal_transactions/1.json
  def show
  end

  # GET /deal_transactions/new
  def new
    @deal_transaction = DealTransaction.new
  end

  # GET /deal_transactions/1/edit
  def edit
  end

  # POST /deal_transactions
  # POST /deal_transactions.json
  def create
    @deal_transaction = DealTransaction.new(deal_transaction_params)

    respond_to do |format|
      if @deal_transaction.save
        format.html { redirect_to @deal_transaction, notice: 'Deal transaction was successfully created.' }
        format.json { render :show, status: :created, location: @deal_transaction }
      else
        format.html { render :new }
        format.json { render json: @deal_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /deal_transactions/1
  # PATCH/PUT /deal_transactions/1.json
  def update
    respond_to do |format|
      if @deal_transaction.update(deal_transaction_params)
        format.html { redirect_to @deal_transaction, notice: 'Deal transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @deal_transaction }
      else
        format.html { render :edit }
        format.json { render json: @deal_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deal_transactions/1
  # DELETE /deal_transactions/1.json
  def destroy
    @deal_transaction.destroy
    respond_to do |format|
      format.html { redirect_to deal_transactions_url, notice: 'Deal transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deal_transaction
      @deal_transaction = DealTransaction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deal_transaction_params
      params.require(:deal_transaction).permit(:transaction_id, :fund_account, :fund_name, :transaction_date_time, :trade_date, :settlement_date, :currency_code, :exchange, :instrument_version, :quantity, :price, :transaction_cost, :consideration, :cash_account, :reversal, :reversed_transaction_id, :bda_branch_code, :bda_partner_code, :deal_source, :isin, :sector, :industry)
    end
end
