class MonthToDate < ApplicationRecord
  self.table_name = "peregrine.func_month_to_date"


  def self.get_all_month_to_date(accounts)

    all_dates = []
    MonthToDate.where(fund_account: accounts).each{|m| all_dates << m.date.strftime("%Y%m") }
    date = all_dates.uniq

    mtd_date_data = {}
    date.each{|d| mtd_date_data[d] = []} 

    mtd_date_data.each do |k, v|
      MonthToDate.where(fund_account: accounts).all.each do |ft|
        if (ft.date.strftime("%Y%m").to_i - k.to_i) == 0
          v << ft.as_json
        end
      end
    end

    # mtd_date_data.each do |k, v|
    #   FinancialTransaction.where(fund_account: accounts).all.each do |ft|
    #     if (ft.value_date.strftime("%Y%m").to_i - k.to_i) == 0
    #       v << ft.as_json
    #     end
    #   end
    # end
  end
    
end
