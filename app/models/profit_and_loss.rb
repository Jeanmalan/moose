class ProfitAndLoss < ApplicationRecord
  self.table_name = "peregrine.func_profit_and_loss"


  def self.get_profit_and_loss_by_account(user_fund_accounts)
    return ProfitAndLoss.all.where(fund_account: user_fund_accounts)
  end  

end
