class PortfolioValuation < ApplicationRecord
    self.table_name = "peregrine.portfolio_valuation"

    def self.get_portfolio_valuation_by_account(account)
        @pv = nil
        account.each do |a|     
            @pv = self.all.where(fund_account: a)
        end
        return @pv.order( 'valuation_date DESC' )
    end

    def self.get_portfolio_valuation_graph_detail(account)
      @details = []
      labels= []
      data = []
      dates = []
      account.each do |a|
        PortfolioValuation.all.where(fund_account: a).map{|x| labels << x.instrument_code_name }
      end
      @details << labels.uniq
      labels = labels.uniq
      #create data structure
      labels.each do |l|
        data << {'label': l, 'data': [], 'backgroundColor': "#21CE99", 'pointBorderColor': "#21CE99","pointHoverBackgroundColor": "#21CE99"}
      end
      #populate data structure
      data.each do |d|
        PortfolioValuation.all.where(fund_account: account).each{|p| (d[:data] << p.wac_native && dates << p.valuation_date.to_s[0,11]) if p.instrument_code_name == d[:label] }
      end
      dates = dates.uniq
      @details << data
      @details << dates.uniq
      return @details
    end

    def self.get_portfolio_valuation_graph_unique_detail(account, name)
      @details = []
      labels= []
      data = []
      dates = []
      PortfolioValuation.all.where(fund_account: account).where(instrument_code_name: name).map{|x| labels << x.instrument_code_name }
      @details << labels.uniq
      labels = labels.uniq
      #create data structure
      labels.each do |l|
        data << {'label': l, 'data': [], 'backgroundColor': "#21CE99", 'pointBorderColor': "#c8f7e8","pointHoverBackgroundColor": "#21CE99"}
      end
      #populate data structure
      data.each do |d|
        PortfolioValuation.all.where(fund_account: account).where(instrument_code_name: name).each{|p| (d[:data] << p.wac_native && dates << p.valuation_date.to_s[0,11]) if p.instrument_code_name == d[:label] }
      end
      dates = dates.uniq
      @details << data
      @details << dates.uniq
      return @details
    end

    def self.portfolio_valuation_by_fund_account_number(current_user, account)
      PortfolioValuation.all.where(fund_account: account)
    end



end
