class FinancialTransaction < ApplicationRecord
  self.table_name = "peregrine.financial_transaction"

  def self.get_transactions_by_date_and_account(account, date)
    return FinancialTransaction.where(fund_account: account).where(value_date: date)
  end
end

