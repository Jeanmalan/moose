class Profile < ApplicationRecord

  def self.get_profit_and_loss_by_user_id(user_id)
    list_fund_accounts = []
    user = User.find(user_id)
    user.agc_definition_tables.map{|x| list_fund_accounts << x.fund_account}
    profit_and_loss = []
    list_fund_accounts.each do |fa|
      profit_and_loss = ProfitAndLoss.all.where(fund_account: fa)
    end
    profit_and_loss
  end

  def self.get_user_fund_accounts(user_id)
    list_fund_accounts = []
    user = User.find(user_id)
    user.agc_definition_tables.map{|x| list_fund_accounts << x.fund_account}

    list_fund_accounts.uniq
  end

end
