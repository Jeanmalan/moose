Rails.application.routes.draw do
  root 'profiles#index'
  resources :profiles
  resources :agc_definition_tables
  devise_for :users
  resources :iress_portfolio_cash_details
  resources :year_to_dates
  resources :month_to_dates 
  resources :profit_and_losses do
    collection do
      get :get_pnl_details
      post :get_profit_and_loss_by_fund_account
    end
  end
  resources :turnovers
  resources :prices
  resources :portfolio_valuations do
    collection do
    post :get_portfoilio_valuation_details  
    post :get_report_graph_portfolio_valuations
    get :get_portfoilio_graph_details
    end
  end
  resources :instruments
  resources :instrument_margin_levels
  resources :fx_rates
  resources :deal_transactions
  resources :client_informations
  resources :client_category_to_instruments
  resources :client_categories
  resources :cash_flow_types
  resources :cash_flow_sub_types
  resources :financial_transactions do
    collection do 
      post :get_ft_by_fund_account_and_date
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
